<?php

namespace ZT\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;


class SecurityController extends Controller
{

    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        if($error=$request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)){

            $error=$request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);

        }else{

            $error=$session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);

        }
        
        $NG = $this->get('number_generator');
        
        $NG->setSize('large');
        
        $days_until_first_achievement= $NG->getImage(102);
    
        return $this->render('ZTSecurityBundle:Security:login.html.twig',array(

            'last_username'=> $session->get(SecurityContext::LAST_USERNAME),
            'error'=>$error,
            'days_until_first_achievement' => $days_until_first_achievement
        ));

    }

    /**
     * @Route("/requestpassword", name="password_request")
     * @Template("ZTSecurityBundle:Security:request.html.twig")
     */
    public function requestPassword(Request $request){

        $user = new \ZT\UserBundle\Entity\User();

        $password_request_type = new \ZT\UserBundle\Form\PasswordRequestType();

        $form = $this->createForm($password_request_type, $user,array('cascade_validation'=>true,'validation_groups' => array('passwordrequest')));

        if($request->getMethod() == "POST"){

            $form->bind($this->getRequest());

            if($form->isValid()){

                $data = $form->getData();

                $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');

                $user = $user_repo->findOneBy(array('email' =>$data->getEmail()));

                if( is_null( $user )){

                    throw new \Exception('user not found');

                }

                $passwordrequest = new \ZT\UserBundle\Entity\PasswordRequest();

                $passwordrequest->setUser( $user );

                $em = $this->getDoctrine()->getEntityManager();

                $em->persist($user);
                $em->persist($passwordrequest);

                $em->flush();


                $emailer = $this->get('emailer');

                $emailer->setTemplate('PASSWORD_RESET_REQUEST',array('user'=>$user,'password_request'=>$passwordrequest ));

                $emailer->send($user);

            }

        }

        return array('form'=> $form->createView());


    }

    /**
     * @Route("/resetpassword/{key}", defaults={"key"=null}, name="password_reset")
     * @Template("ZTSecurityBundle:Security:reset.html.twig")
     */
    public function resetPassword(Request $request, $key){

        if( is_null($key) && $request->getMethod() == "GET" ){

            throw new \Exception('no key given');

        }
        $password_request_repo = $this->getDoctrine()->getRepository('ZTUserBundle:PasswordRequest');

        try{

            $password_request = $password_request_repo->findOneBy(array('reset_key' =>$key));

        }catch(\Exception $e){

            throw $e;
        }

        if( is_null( $password_request )){

            throw new \Exception('password reset not found');

        }

        $user = $password_request->getUser();

        $password_reset_type = new \ZT\UserBundle\Form\PasswordResetType();

        $form = $this->createForm($password_reset_type, $user,array('cascade_validation'=>true,'validation_groups' => array('passwordreset')));

        if($request->getMethod() == "POST"){

            $form->bind($this->getRequest());

            if($form->isValid()){

                $user = $form->getData();



                $factory = $this->get('security.encoder_factory');

                $encoder = $factory->getEncoder($user);

                $password = $encoder->encodePassword($user->getPassword() ,$user->getSalt());

                $user->setPassword($password);

                $em = $this->getDoctrine()->getEntityManager();

                $em->persist($user);

                $em->remove($password_request);

                $em->flush();



            }

        }

        return array(
            'key' => $key,
            'form'=> $form->createView());


    }
}
