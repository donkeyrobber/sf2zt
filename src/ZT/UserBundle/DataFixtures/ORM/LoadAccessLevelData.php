<?php

namespace ZT\UserBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ZT\UserBundle\Entity\AccessLevel;


class LoadAccessLevelData implements FixtureInterface, ContainerAwareInterface{


    private $container;

    public function setContainer(ContainerInterface $container = null){

        $this->container = $container;

    }

    public function load(ObjectManager $manager){

        $em = $this->container->get('doctrine.orm.entity_manager');

        $access_level_repo = $em->getREpository('ZTUserBundle:AccessLevel');

        $access_levels = array(
                            array('access_level' =>'Public','label' =>'Everyone'),
                            array('access_level' =>'Private','label' =>'Just me'),        
        );

        foreach($access_levels as $al){

            if( is_null($access_level_repo->findOneBy(array('access_level'=> $al['access_level']))) ){

                $access_level = new AccessLevel();

                $access_level->setAccessLevel($al['access_level']);
                $access_level->setlabel($al['label']);
                $manager->persist($access_level);

            }
        
        }

        $manager->flush();
        

    }

}
