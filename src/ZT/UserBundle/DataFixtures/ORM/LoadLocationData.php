<?php

namespace ZT\UserBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ZT\UserBundle\Entity\Location;


class LoadLocationData implements FixtureInterface, ContainerAwareInterface{


    private $container;

    public function setContainer(ContainerInterface $container = null){

        $this->container = $container;

    }

    public function load(ObjectManager $manager){

        $em = $this->container->get('doctrine.orm.entity_manager');

        $location_repo = $em->getRepository('ZTUserBundle:Location');

        $locations = array(
                            'Essex',
                            'London',
                            'Hertfordshire',
                            'Middlesex',
                            'Berkshire',
                            'Surrey',
                            'East Sussex',
                            'West Sussex',
                            'Kent',
                            'Suffolk',
                            'Norfolk',
                            'Cambridgeshire',
                            'Lincolnshire',
                            'Oxfordshire',
                            'Herefordshire',
                            'Yorkshire'
        
        );

        foreach($locations as $l){

            if( is_null($location_repo->findOneBy(array('location'=> $l))) ){

                $location = new Location();

                $location->setLocation($l);

                $manager->persist($location);

            }
        
        }

        $manager->flush();
        

    }

}
