<?php

namespace ZT\UserBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ZT\UserBundle\Entity\GoalCategory;


class LoadGoalCategoryData implements FixtureInterface, ContainerAwareInterface{


    private $container;

    public function setContainer(ContainerInterface $container = null){

        $this->container = $container;

    }

    public function load(ObjectManager $manager){


        $em = $this->container->get('doctrine.orm.entity_manager');

        $goal_category_repo = $em->getRepository('ZTUserBundle:GoalCategory');


        $goal_categories = array(
                            'Health',
                            'Wealth',
                            'Leisure'        
        );

        foreach($goal_categories as $gc){

            if( is_null($goal_category_repo->findOneBy(array('goal_category'=> $gc))) ){

                $goal_category = new GoalCategory();

                $goal_category->setGoalCategory($gc);

                $manager->persist($goal_category);

            }
        }

        $manager->flush();
        

    }

}
