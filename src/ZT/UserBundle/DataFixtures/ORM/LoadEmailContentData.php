<?php

namespace ZT\UserBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

use ZT\UserBundle\Entity\EmailContent;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadEmailContentData implements FixtureInterface, ContainerAwareInterface{


    private $container;

    public function setContainer(ContainerInterface $container = null){

        $this->container = $container;

    }

    public function load(ObjectManager $manager){

        // NEW POST ON YOUR GOAL

        $em = $this->container->get('doctrine.orm.entity_manager');

        $email_content_repo = $em->getREpository('ZTUserBundle:EmailContent');

        if( is_null( $email_content_repo->findOneBy(array('tag'=> 'NEW_POST_ON_YOUR_GOAL')) ) ){

            $emailContent = new EmailContent();

            $emailContent->setTag('NEW_POST_ON_YOUR_GOAL');
            $emailContent->setReplyTo('no-reply@zimtam.com');
            $emailContent->setContentType('text/html');
            $emailContent->setFrom('updates@zimtam.com');
            $emailContent->setSubject('You have a new post on your goal!');

            $contentTwigFile = file_get_contents(__DIR__.'/../../Resources/views/Emails/new_post_on_your_goal.html.twig');

            $emailContent->setBody($contentTwigFile );

            $manager->persist($emailContent);

            $manager->flush();

        }

        if( is_null( $email_content_repo->findOneBy(array('tag'=> 'NEW_SUPPORTER_ON_YOUR_GOAL')) ) ){

            $emailContent = new EmailContent();

            $emailContent->setTag('NEW_SUPPORTER_ON_YOUR_GOAL');
            $emailContent->setReplyTo('no-reply@zimtam.com');
            $emailContent->setContentType('text/html');
            $emailContent->setFrom('updates@zimtam.com');
            $emailContent->setSubject('Your goal has a new supporter!');

            $contentTwigFile = file_get_contents(__DIR__.'/../../Resources/views/Emails/new_supporter_on_your_goal.html.twig');

            $emailContent->setBody($contentTwigFile );

            $manager->persist($emailContent);

            $manager->flush();

        }

        if( is_null( $email_content_repo->findOneBy(array('tag'=> 'PASSWORD_RESET_REQUEST')) ) ){

            $emailContent = new EmailContent();

            $emailContent->setTag('PASSWORD_RESET_REQUEST');
            $emailContent->setReplyTo('no-reply@zimtam.com');
            $emailContent->setContentType('text/html');
            $emailContent->setFrom('updates@zimtam.com');
            $emailContent->setSubject('Your password reset');

            $contentTwigFile = file_get_contents(__DIR__.'/../../Resources/views/Emails/password_reset_request.html.twig');

            $emailContent->setBody($contentTwigFile );

            $manager->persist($emailContent);

        }

        $manager->flush();
        //

    }

}
