<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use ZT\UserBundle\Entity\User;
use JMS\Serializer\Annotation as Serialize;


/**
 * ZT\UserBundle\Entity\Location
 *
 * @ORM\Table()
 * @ORM\Entity
 * @Serialize\ExclusionPolicy("all")  
 */
class Location
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $location
     *
     * @ORM\Column(name="location", type="string", length=255)
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list"})     
     */
    protected $location;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

    /**
     * 
     * @ORM\OneToMany(targetEntity="User", mappedBy="location")
     */

    protected $users;


    /**
     * Get id
     *
     * @return integer 
     */
     
    public function __construct(){
    
        $this->isActive = true;
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();    
    }
     
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    
        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }


    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Location
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add users
     *
     * @param ZT\UserBundle\Entity\User $users
     * @return Location
     */
    public function addUser(\ZT\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;
    
        return $this;
    }

    /**
     * Remove users
     *
     * @param ZT\UserBundle\Entity\User $users
     */
    public function removeUser(\ZT\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}