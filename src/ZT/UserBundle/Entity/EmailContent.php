<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailContent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ZT\UserBundle\Entity\EmailContentRepository")
 */
class EmailContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="text")
     *
     */

    private $tag;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="text")
     *
     */

    private $subject;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="text")
     *
     */

    private $body;
    /**
     *
     * @var string
     *
     * @ORM\Column(type="text")
     *
     */

    private $reply_to;


    /**
     *
     * @var string
     *
     * @ORM\Column(type="text")
     *
     */

    private $content_type;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="email_from",type="text")
     *
     */

    private $from;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="datetime")
     *
     */

    private $last_modified;

    /**
     * Get id
     *
     * @return integer
     *
     */

    public function __construct(){

        $this->setLastModified( new \DateTime());

    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return EmailContent
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return EmailContent
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set reply_to
     *
     * @param string $replyTo
     * @return EmailContent
     */
    public function setReplyTo($replyTo)
    {
        $this->reply_to = $replyTo;
    
        return $this;
    }

    /**
     * Get reply_to
     *
     * @return string 
     */
    public function getReplyTo()
    {
        return $this->reply_to;
    }

    /**
     * Set content_type
     *
     * @param string $contentType
     * @return EmailContent
     */
    public function setContentType($contentType)
    {
        $this->content_type = $contentType;
    
        return $this;
    }

    /**
     * Get content_type
     *
     * @return string 
     */
    public function getContentType()
    {
        return $this->content_type;
    }

    /**
     * Set from
     *
     * @param string $from
     * @return EmailContent
     */
    public function setFrom($from)
    {
        $this->from = $from;
    
        return $this;
    }

    /**
     * Get from
     *
     * @return string 
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailContent
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set last_modified
     *
     * @param \DateTime $lastModified
     * @return EmailContent
     */
    public function setLastModified($lastModified)
    {
        $this->last_modified = $lastModified;
    
        return $this;
    }

    /**
     * Get last_modified
     *
     * @return \DateTime 
     */
    public function getLastModified()
    {
        return $this->last_modified;
    }
}