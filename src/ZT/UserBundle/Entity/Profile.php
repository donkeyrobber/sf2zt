<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ZT\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use ZT\UserBundle\Entity\Image;
use JMS\Serializer\Annotation as Serialize;
/**
 * ZT\UserBundle\Entity\Profile
 *
 * @ORM\Table()
 * @ORM\Entity
 * @Serialize\ExclusionPolicy("all")  
 */
class Profile
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     * @ORM\OneToOne(targetEntity="User", inversedBy="profile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")   
     */


    protected $user;


    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\Image")
     * @ORM\OneToOne(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id") 
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list","post_list"}) 
     * @Serialize\Accessor("getImage")                 
     */


    protected $image;

    /**
     * @var string $introduction
     *
     * @ORM\Column(type="string", length=1024, nullable=true)

     */
 
    protected $introduction;


    /**
     * Get id
     *
     * @return integer 
     */

    /**
     * @var string $createdOn
     *
     * @ORM\Column(type="datetime")

     */
 
    protected $createdOn;

    /**
     *  
     * default images     
     *
     */              

     private $default_image = '/img/default_profile_image.png';

    
    public function __construct(){
    
        $this->createdOn = new \DateTime();
    } 


    public function getId()
    {
        return $this->id;
    }


    /**
     * Set user
     *
     * @param ZT\UserBundle\Entity\User $user
     * @return Profile
     */
    public function setUser(\ZT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return ZT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set image
     *
     * @param ZT\UserBundle\Entity\Image $image
     * @return Profile
     */
    public function setImage(\ZT\UserBundle\Entity\Image $image = null)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return ZT\UserBundle\Entity\Image 
     */
    public function getImage()
    {
   
        if(is_null($this->image)){
        
          $this->image = new Image();
          
          $this->image->setPath( $this->default_image ); 
        
        }
           
        return $this->image;
    }

    /**
     * Set introduction
     *
     * @param string $introduction
     * @return Profile
     */
    public function setIntroduction($introduction)
    {
        $this->introduction = $introduction;
    
        return $this;
    }

    /**
     * Get introduction
     *
     * @return string 
     */
    public function getIntroduction()
    {
        return $this->introduction;
    }
    
    public function getDaysOld(){

        $interval = $this->createdOn->diff(new \DateTime() );



        return $interval->format('%a');
    
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return Profile
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    
        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}