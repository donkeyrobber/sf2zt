<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ZT\UserBundle\Entity\Image;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serialize;
/**
 * ZT\UserBundle\Entity\Goal
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ZT\UserBundle\Entity\GoalRepository")
 * @Serialize\ExclusionPolicy("all") 
 */
class Goal
{

    const TYPE = "goal";

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list", "goal_feed"})
     */
    protected $id;

    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="goals")
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list"})     
     */

    protected $user;
    /**
     * @var string $title
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list", "goal_feed"})
     */
    protected $title;

    /**
     * @var string $description
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")       
     */
    protected $description;

    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\AccessLevel")
     * @ORM\ManyToOne(targetEntity="AccessLevel", inversedBy="goals")          
     */

    protected $access_level;


    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\GoalCategory")
     * @ORM\ManyToOne(targetEntity="GoalCategory", inversedBy="goals")      
     */

    protected $goal_category;


    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\Image")
     * @ORM\OneToOne(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list"})
     * @Serialize\Accessor("getImage")                
     */

    protected $image;

    /**
     * @var \DateTime $created_on
     *
     * @ORM\Column(name="created_on", type="date")
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list"})     
       
     */
    protected $created_on;
    
    /**
     * @var int timeframe (days)
     *         
     * @ORM\Column(name="timeframe", type="integer")
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list"})
     */
      
    protected $timeframe;

    /**
     * @var int due date (datetime)

     * @Serialize\Expose()
     * @Serialize\Groups({"goal_feed"})
     * @Serialize\SerializedName("start")
     * @Serialize\Accessor("getDueDate")
     */

    protected $due_date;

    /**
     * @var boolean active
     * 
     * @ORM\Column(name="isActive", type="boolean")          
     */         


    protected $isActive;


    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="supported_goals")
 
     */

    protected $supporters;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="goal")
     */


    protected $posts;


    /**
     *  
     * default images     
     *
     */              

     private $default_image = array(
     
        'health' =>'/img/health.png',
        'wealth' =>'/img/wealth.png',
        'leisure' =>'/img/leisure.png',  
     
     );

    public function __sleep()
    {
        return array('id');

    }



    public function __construct(){
        $this->created_on = new \DateTime();
        $this->isActive = true;
        $this->image = null;
//         $this->supporters = new ArrayCollection();
//         $this->posts = new ArrayCollection();
    }    

    /**
     * Get image
     *
     * @return ZT\UserBundle\Entity\Image 
     */
    public function getImage()
    {

        $image = $this->image;

        if(isset($this->goal_category) ){

          $image = new Image();

          $category=$this->goal_category->getGoalCategory();
          
          $image->setPath( $this->default_image[strtolower($category)] );

        }
    
        return $image;
    }

    
    public function getDaysRemaining(){
    
       // $end_date = $this->created_on->add( new \DateInterval('P'.$this->timeframe.'D'));

        $end_date = $this->getDueDate();

        $today = new \DateTime();

        return $today->diff($end_date)->days;
    
    }
    public function getDueDate(){

        return $this->created_on->add( new \DateInterval('P'.$this->timeframe.'D'));


    }
    public function getDaysOld(){
    
        return $this->created_on->diff(new \DateTime() )->days;    
    
    }    

    public function getType(){
    
        return static::TYPE;
    
    } 


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Goal
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Goal
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created_on
     *
     * @param \DateTime $createdOn
     * @return Goal
     */
    public function setCreatedOn($createdOn)
    {
        $this->created_on = $createdOn;
    
        return $this;
    }

    /**
     * Get created_on
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * Set timeframe
     *
     * @param integer $timeframe
     * @return Goal
     */
    public function setTimeframe($timeframe)
    {
        $this->timeframe = $timeframe;
    
        return $this;
    }

    /**
     * Get timeframe
     *
     * @return integer 
     */
    public function getTimeframe()
    {
        return $this->timeframe;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Goal
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set user
     *
     * @param \ZT\UserBundle\Entity\User $user
     * @return Goal
     */
    public function setUser(\ZT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \ZT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set access_level
     *
     * @param \ZT\UserBundle\Entity\AccessLevel $accessLevel
     * @return Goal
     */
    public function setAccessLevel(\ZT\UserBundle\Entity\AccessLevel $accessLevel = null)
    {
        $this->access_level = $accessLevel;
    
        return $this;
    }

    /**
     * Get access_level
     *
     * @return \ZT\UserBundle\Entity\AccessLevel 
     */
    public function getAccessLevel()
    {
        return $this->access_level;
    }

    /**
     * Set goal_category
     *
     * @param \ZT\UserBundle\Entity\GoalCategory $goalCategory
     * @return Goal
     */
    public function setGoalCategory(\ZT\UserBundle\Entity\GoalCategory $goalCategory = null)
    {
        $this->goal_category = $goalCategory;
    
        return $this;
    }

    /**
     * Get goal_category
     *
     * @return \ZT\UserBundle\Entity\GoalCategory 
     */
    public function getGoalCategory()
    {
        return $this->goal_category;
    }

    /**
     * Set image
     *
     * @param \ZT\UserBundle\Entity\Image $image
     * @return Goal
     */
    public function setImage(\ZT\UserBundle\Entity\Image $image = null)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Add supporters
     *
     * @param \ZT\UserBundle\Entity\User $supporters
     * @return Goal
     */
    public function addSupporter(\ZT\UserBundle\Entity\User $supporters)
    {
        $this->supporters[] = $supporters;
    
        return $this;
    }

    /**
     * Remove supporters
     *
     * @param \ZT\UserBundle\Entity\User $supporters
     */
    public function removeSupporter(\ZT\UserBundle\Entity\User $supporters)
    {
        $this->supporters->removeElement($supporters);
    }

    /**
     * Get supporters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupporters()
    {
        return $this->supporters;
    }

    /**
     * is supported by
     *
     * @param \ZT\UserBundle\Entity\User $supporters
     * @return bool     
     */
    public function isSupportedBy(\ZT\UserBundle\Entity\User $supporters)
    {
        return $this->supporters->contains($supporters);
    }

    /**
     * Add posts
     *
     * @param \ZT\UserBundle\Entity\Post $posts
     * @return Goal
     */
    public function addPost(\ZT\UserBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;
    
        return $this;
    }

    /**
     * Remove posts
     *
     * @param \ZT\UserBundle\Entity\Post $posts
     */
    public function removePost(\ZT\UserBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }
}