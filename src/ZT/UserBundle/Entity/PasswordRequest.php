<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * PasswordReset
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PasswordRequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column( type="string", length=64)
     * @Assert\NotBlank()
     */
    private $reset_key;

    /**
     * @var string $createdOn
     *
     * @ORM\Column(type="datetime")

     */

    private $createdOn;

    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     * @ORM\OneToOne(targetEntity="User", inversedBy="passwordrequest")
     */


    protected $user;

    /**
     * Get id
     *
     * @return integer 
     */

    public function __construct(){

        $this->createdOn = new \DateTime();
        $this->reset_key = substr(base64_encode(sha1(mt_rand())), 0, 32);

    }

    public function getId()
    {
        return $this->id;
    }


    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return PasswordReset
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    
        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set user
     *
     * @param \ZT\UserBundle\Entity\User $user
     * @return PasswordReset
     */
    public function setUser(\ZT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \ZT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set reset_key
     *
     * @param string $resetKey
     * @return PasswordReset
     */
    public function setResetKey($resetKey)
    {
        $this->reset_key = $resetKey;
    
        return $this;
    }

    /**
     * Get reset_key
     *
     * @return string 
     */
    public function getResetKey()
    {
        return $this->reset_key;
    }
}