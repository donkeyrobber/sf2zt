<?php

 namespace ZT\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use JMS\Serializer\Annotation as Serialize;
use ZT\UserBundle\Entity\EmailSent;

/**
 * @ORM\Entity(repositoryClass="ZT\UserBundle\Entity\UserRepository")
 */
/**
 * ZT\UserBundle\Entity\User
 *  
 * @ORM\Table()
 * @ORM\Entity  

 * @DoctrineAssert\UniqueEntity(fields="email", message="This email address is already registered on our system", groups={"registration","profile"})
 * @DoctrineAssert\UniqueEntity(fields="username",  message="This username address is already registered on our system", groups={"registration","profile"})
 * @Serialize\ExclusionPolicy("all") 
 */
class User implements UserInterface
{

    const TYPE = "user";

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serialize\Expose() 
     * @Serialize\Groups({"post_list"})     
     */
    protected $id;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true, unique=true)
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list","post_list"})
     *
     */
    protected $username;
    
    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Email(groups={"passwordrequest"})
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list"})
     */
    protected $email;
    
    /**
     *@var string $salt
     *
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank()
     */
    protected $salt;
    
    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=40)
     * @Assert\NotBlank(groups={"registration","passwordreset"})
     */
    protected $password;
  
    /**
     *@var string $isActive
     *
     *@ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

        
    /**
     * @var string $first_name
     *
     * @ORM\Column(type="string", length=50,nullable=true)    
     */

    protected $first_name;        
    /**
     * @var string $last_name
     *
     * @ORM\Column(type="string", length=50,nullable=true)

     */
    protected $last_name;

        
    /**
     * @var string $gender
     *
     * @ORM\Column(type="string", length=10, nullable=true)

     */
    protected $gender;

        

        
    /**
     * @var string $date_of_birth
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotBlank(groups={"profile"})
     */
    protected $date_of_birth;

    /**
     * @var ZT\UserBundle\Entity\Location $location
     *
     * @Assert\Type(type="ZT\UserBundle\Entity\Location")
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="users")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list"}) 
     */

    protected $location;

    /**
     * @var ZT\UserBundle\Entity\Profile $profile
     * @ORM\OneToOne(targetEntity="Profile", mappedBy="user", cascade={"persist"})
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list","post_list"})         
     *          
     */

    protected $profile;

    /**
     * @var ZT\UserBundle\Entity\Goal $goal
     *
     * @Assert\Type(type="ZT\UserBundle\Entity\Goal")
     * @ORM\OneToMany(targetEntity="Goal", mappedBy="user")
   
     */

    protected $goals;

    /**
     * @var string $createdOn
     *
     * @ORM\Column(type="datetime")

     */
 
    protected $createdOn;
    
    /**
     * @var ZT\UserBundle\Entity\Goal $goal
     *
     * @Assert\Type(type="ZT\UserBundle\Entity\Goal")
     * @ORM\ManyToMany(targetEntity="Goal", mappedBy="supporters")
     */
     
     protected $supported_goals;

    
    /**
     * @var ZT\UserBundle\Entity\User $user
     *
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     * @ORM\ManyToMany(targetEntity="User", mappedBy="friends_by")
     */
     
     protected $friends_with;
    
    
    /**
     * @var ZT\UserBundle\Entity\User $user
     *
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     * @ORM\ManyToMany(targetEntity="User", inversedBy="friends_with")
     * @ORM\JoinTable(name="friends",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="friend_user_id", referencedColumnName="id")}
     *      )     
     */
     
     protected $friends_by;
     
    
    /**
     * @var ZT\UserBundle\Entity\Post $post
     *
     * @Assert\Type(type="ZT\UserBundle\Entity\Post")
     * @ORM\OneToMany(targetEntity="Post", mappedBy="supporter")
     */
     
     protected $posts;     
     

    /**
     * @var string $identifier
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list","post_list"})         
     * @Serialize\Accessor("getIdentifier")           
     */

    protected $identifier;

    /**
     * @var string $profileIdentifier
     * @Serialize\Expose()
     * @Serialize\Groups({"goal_list"})
     * @Serialize\Accessor("getProfileIdentifier")
     */

    protected $profile_identifier;

    /**
     * @var string $emails_sent
     * @Assert\Type(type="ZT\UserBundle\Entity\EmailSent")
     * @ORM\OneToMany(targetEntity="EmailSent", mappedBy="user")
     */

    protected $emails_sent;

    /**
     * @var string $messages_from
     * @Assert\Type(type="ZT\UserBundle\Entity\Message")
     * @ORM\OneToMany(targetEntity="Message", mappedBy="from")
     */

    protected $messages_from;

    /**
     * @var string $messages_to
     * @Assert\Type(type="ZT\UserBundle\Entity\Message")
     * @ORM\OneToMany(targetEntity="Message", mappedBy="to")
     */

    protected $messages_to;


    /**
     * @var ZT\UserBundle\Entity\PasswordRequest $passwordrequest
     * @ORM\OneToOne(targetEntity="PasswordRequest", mappedBy="user", cascade={"persist"})
     *
     */

    protected $passwordrequest;

    public function __sleep()
    {
        return array('id');
    }
    
    public function __construct(){

        $this->salt= md5(uniqid(null,true));
        $this->isActive = true;
        $this->goals= new ArrayCollection();    
        $this->createdOn = new \DateTime();
        $this->supported_goals = new ArrayCollection();
        $this->friends_with = new ArrayCollection();
       
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return e mail
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function getRoles(){

        return array('ROLE_USER');
 
    }

     /**
     * Get password
     *
     * @Attribute(Arrayable=false)     
     * @return password
     */

    public function getPassword(){

        return $this->password;

    }   
     

    public function setPassword($password){

        $this->password=$password;

    }   

    public function getSalt(){

        return $this->salt;

    } 

    public function eraseCredentials(){} 

    public function equals(UserInterface $user){

        return $this->id == $user->getId();

    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }


    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    
        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    
        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

 
    /**
     * Set date_of_birth
     *
     * @param \DateTime $dateOfBirth
     * @return User
     */
    public function setDateOfBirth(\DateTime $dateOfBirth)
    {
        $this->date_of_birth = $dateOfBirth;
    
        return $this;
    }

    /**
     * Get date_of_birth
     *
     * @return \DateTime 
     */
    public function getDateOfBirth()
    {
        return $this->date_of_birth;
    }


    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set location
     *
     * @param integer $location
     * @return User
     */
    public function setLocation($location)
    {
        $this->location = $location;
    
        return $this;
    }

    /**
     * Get location
     *
     * @return integer 
     */
    public function getLocation()
    {
        return $this->location;
    }


    /**
     * Set profile
     *
     * @param ZT\UserBundle\Entity\Profile $profile
     * @return User
     */
    public function setProfile(\ZT\UserBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;
    
        return $this;
    }

    /**
     * Get profile
     *
     * @return ZT\UserBundle\Entity\Profile 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Add goals
     *
     * @param ZT\UserBundle\Entity\Goal $goals
     * @return User
     */
    public function addGoal(\ZT\UserBundle\Entity\Goal $goals)
    {
        $this->goals[] = $goals;
    
        return $this;
    }

    /**
     * Remove goals
     *
     * @param ZT\UserBundle\Entity\Goal $goals
     */
    public function removeGoal(\ZT\UserBundle\Entity\Goal $goals)
    {
        $this->goals->removeElement($goals);
    }

    /**
     * Get goals
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return User
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    
        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
    
    public function getDaysOld( $as_array = false ){

        $interval = $this->date_of_birth->diff(new \DateTime() );



        return $interval->format('%a');
    
    }


    public function getProfileIdentifier(){
    
        return isset($this->username) ? $this->username : $this->id;
    
    }

    public function getIdentifier(){
    
        return isset($this->username) ? $this->username : $this->email;
    
    }
    

    /**
     * Add supported_goals
     *
     * @param \ZT\UserBundle\Entity\Goal $supportedGoals
     * @return User
     */
    public function addSupportedGoal(\ZT\UserBundle\Entity\Goal $supportedGoals)
    {
        $this->supported_goals[] = $supportedGoals;
    
        return $this;
    }

    /**
     * Remove supported_goals
     *
     * @param \ZT\UserBundle\Entity\Goal $supportedGoals
     */
    public function removeSupportedGoal(\ZT\UserBundle\Entity\Goal $supportedGoals)
    {
        $this->supported_goals->removeElement($supportedGoals);
    }

    /**
     * Get supported_goals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupportedGoals()
    {
        return $this->supported_goals;
    }

    /**
     * Add friends_with
     *
     * @param \ZT\UserBundle\Entity\User $friendsWith
     * @return User
     */
    public function addFriendsWith(\ZT\UserBundle\Entity\User $friendsWith)
    {
        $this->friends_with[] = $friendsWith;
    
        return $this;
    }

    /**
     * Remove friends_with
     *
     * @param \ZT\UserBundle\Entity\User $friendsWith
     */
    public function removeFriendsWith(\ZT\UserBundle\Entity\User $friendsWith)
    {
        $this->friends_with->removeElement($friendsWith);
    }

    /**
     * Get friends_with
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFriendsWith()
    {
        return $this->friends_with;
    }
    
    /**
     * is friends_with

     * @param \ZT\UserBundle\Entity\User $friend
     * @return boolean 
     */
    public function isFriendsWith(\ZT\UserBundle\Entity\User $friend)
    {
        return $this->friends_with->contains($friend);
    }



    /**
     * Add friends_by
     *
     * @param \ZT\UserBundle\Entity\User $friendsBy
     * @return User
     */
    public function addFriendsBy(\ZT\UserBundle\Entity\User $friendsBy)
    {
        $this->friends_by[] = $friendsBy;
    
        return $this;
    }

    /**
     * Remove friends_by
     *
     * @param \ZT\UserBundle\Entity\User $friendsBy
     */
    public function removeFriendsBy(\ZT\UserBundle\Entity\User $friendsBy)
    {
        $this->friends_by->removeElement($friendsBy);
    }

    /**
     * Get friends_by
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFriendsBy()
    {
        return $this->friends_by;
    }

    /**
     * Add goal_posts
     *
     * @param \ZT\UserBundle\Entity\Post $posts
     * @return User
     */
    public function addPost(\ZT\UserBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;
    
        return $this;
    }

    /**
     * Remove goal_posts
     *
     * @param \ZT\UserBundle\Entity\Post $posts
     */
    public function removePost(\ZT\UserBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get goal_posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }
    
    public function getGoalCount()
    {
        return count($this->goals);
    }
    
    public function getSupportedGoalCount()
    {
        return count($this->supported_goals);
    }    
    public function getFriendCount()
    {
        return count($this->friends_with);
    }
    
    public function getType(){
    
        return static::TYPE;
    
    }


    /**
     * Add emails_sent
     *
     * @param \ZT\UserBundle\Entity\EmailSent $emailsSent
     * @return User
     */
    public function addEmailsSent(\ZT\UserBundle\Entity\EmailSent $emailsSent)
    {
        $this->emails_sent[] = $emailsSent;
    
        return $this;
    }

    /**
     * Remove emails_sent
     *
     * @param \ZT\UserBundle\Entity\EmailSent $emailsSent
     */
    public function removeEmailsSent(\ZT\UserBundle\Entity\EmailSent $emailsSent)
    {
        $this->emails_sent->removeElement($emailsSent);
    }

    /**
     * Get emails_sent
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmailsSent()
    {
        return $this->emails_sent;
    }

    /**
     * Add messages_from
     *
     * @param \ZT\UserBundle\Entity\Message $messagesFrom
     * @return User
     */
    public function addMessagesFrom(\ZT\UserBundle\Entity\Message $messagesFrom)
    {
        $this->messages_from[] = $messagesFrom;
    
        return $this;
    }

    /**
     * Remove messages_from
     *
     * @param \ZT\UserBundle\Entity\Message $messagesFrom
     */
    public function removeMessagesFrom(\ZT\UserBundle\Entity\Message $messagesFrom)
    {
        $this->messages_from->removeElement($messagesFrom);
    }

    /**
     * Get messages_from
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessagesFrom()
    {
        return $this->messages_from;
    }

    /**
     * Add messages_to
     *
     * @param \ZT\UserBundle\Entity\Message $messagesTo
     * @return User
     */
    public function addMessagesTo(\ZT\UserBundle\Entity\Message $messagesTo)
    {
        $this->messages_to[] = $messagesTo;
    
        return $this;
    }

    /**
     * Remove messages_to
     *
     * @param \ZT\UserBundle\Entity\Message $messagesTo
     */
    public function removeMessagesTo(\ZT\UserBundle\Entity\Message $messagesTo)
    {
        $this->messages_to->removeElement($messagesTo);
    }

    /**
     * Get messages_to
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessagesTo()
    {
        return $this->messages_to;
    }

    /**
     * Set passwordrequest
     *
     * @param \ZT\UserBundle\Entity\PasswordRequest $passwordrequest
     * @return User
     */
    public function setPasswordrequest(\ZT\UserBundle\Entity\PasswordRequest $passwordrequest = null)
    {
        $this->passwordrequest = $passwordrequest;
    
        return $this;
    }

    /**
     * Get passwordrequest
     *
     * @return \ZT\UserBundle\Entity\PasswordRequest 
     */
    public function getPasswordrequest()
    {
        return $this->passwordrequest;
    }
}