<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ZT\UserBundle\Entity\Message;

/**
 * Conversation
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Conversation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var created_on
     *
     * @ORM\Column(type="datetime")
     */

    private $created_on;

    /**
     * @var messages
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="conversation")
     *
     */

    private $messages;

    public function __construct(){

        $this->created_on = new \DateTime();

    }

    /**
     * Get id
     *
     * @return integer
     */

    public function getId()
    {
        return $this->id;
    }


    /**
     * Set created_on
     *
     * @param \DateTime $createdOn
     * @return Conversation
     */
    public function setCreatedOn($createdOn)
    {
        $this->created_on = $createdOn;
    
        return $this;
    }

    /**
     * Get created_on
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * Add messages
     *
     * @param \ZT\UserBundle\Entity\Message $messages
     * @return Conversation
     */
    public function addMessage(\ZT\UserBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \ZT\UserBundle\Entity\Message $messages
     */
    public function removeMessage(\ZT\UserBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }
}