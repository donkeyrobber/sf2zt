<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serialize;

/**
 * ZT\UserBundle\Entity\Image
 * @ORM\MappedSuperclass  
 * @ORM\Table()
 * @ORM\Entity
 * @Serialize\ExclusionPolicy("all")  
 */
class Image
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string $path
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serialize\Expose() 
     * @Serialize\Groups({"goal_list", "post_list"})     
     */

    protected $path;


    /**
     * @var file $file
     *
     * @Assert\File(maxSize="1000000")
     */

    protected $file;


    public function __toString(){
    
        return $this->getWebPath();
    
    }

    public function getAbsolutePath(){

        return ($this->path === null ) ? $this->getUploadRootDir() : $this->path ;

    }

    public function getWebPath(){

        return ($this->path === null ) ? $this->getUploadDir() : $this->path ;

    }

    public function getUploadRootDir(){

        return __DIR__."/../../../../web/".$this->getUploadDir();

    } 

    public function getUploadDir(){

        return "/uploads/images";

    }

    /**
     * Set name
     *
     * @param string $name
     * @return Image
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Image
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
    
        return $this->path;
    
    }


    /**
     * Get file
     *
     * @return File 
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Set file
     *
     * @return string File 
     */
    public function setFile($file)
    {
        $this->file=$file;

        return $this;
    }

    public function upload(){

        if( $this->file === null ){

            return;

        }

        $this->file->move($this->getUploadRootDir(), $this->file->getClientOriginalName() );

        $this->path =  $this->getUploadDir()."/".$this->file->getClientOriginalName();	

        $this->file = null;
    }

}