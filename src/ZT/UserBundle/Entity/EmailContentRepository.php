<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * EmailContentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EmailContentRepository extends EntityRepository
{
}
