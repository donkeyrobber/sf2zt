<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Message
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ZT\UserBundle\Entity\MessageRepository")
 */
class Message
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var integer
     *
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages_from")
     */

    private $from;


    /**
     * @var integer
     *
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages_to")
     */

    private $to;



    /**

     * @ORM\ManyToOne(targetEntity="Conversation", inversedBy="messages", cascade={"persist"})
     */

    private $conversation;



    /**
     * @var integer
     *
     * @ORM\Column(type="text")
     */
    private $subject;

    /**
     * @var integer
     *
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_read", type="boolean")
     */
    private $read;

    /**
     * @var integer
     *
     * @ORM\Column(type="datetime")
     */
    private $created_on;


    /**
     * @var integer
     *
     * @ORM\Column(type="boolean")
     */
    private $is_active;


    public function __construct(){

        $this->read=0;
        $this->created_on= new \DateTime();
        $this->is_active = 1;

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set subject
     *
     * @param string $subject
     * @return Message
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set read
     *
     * @param \tinyint $read
     * @return Message
     */
    public function setRead(\tinyint $read)
    {
        $this->read = $read;
    
        return $this;
    }

    /**
     * Get read
     *
     * @return \tinyint 
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Set created_on
     *
     * @param \DateTime $createdOn
     * @return Message
     */
    public function setCreatedOn($createdOn)
    {
        $this->created_on = $createdOn;
    
        return $this;
    }

    /**
     * Get created_on
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * Get from
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Get to
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set from
     *
     * @param \ZT\UserBundle\Entity\User $from
     * @return Message
     */
    public function setFrom(\ZT\UserBundle\Entity\User $from = null)
    {
        $this->from = $from;
    
        return $this;
    }

    /**
     * Set to
     *
     * @param \ZT\UserBundle\Entity\User $to
     * @return Message
     */
    public function setTo(\ZT\UserBundle\Entity\User $to = null)
    {
        $this->to = $to;
    
        return $this;
    }

    /**
     * Set is_active
     *
     * @param \tinyint $isActive
     * @return Message
     */
    public function setIsActive(\tinyint $isActive)
    {
        $this->is_active = $isActive;
    
        return $this;
    }

    /**
     * Get is_active
     *
     * @return \tinyint 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Message
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }



    /**
     * Set conversation
     *
     * @param \ZT\UserBundle\Entity\Conversation $conversation
     * @return Message
     */
    public function setConversation(\ZT\UserBundle\Entity\Conversation $conversation = null)
    {
        $this->conversation = $conversation;
    
        return $this;
    }

    /**
     * Get conversation
     *
     * @return \ZT\UserBundle\Entity\Conversation 
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    public function equals( \ZT\UserBundle\Entity\Message $message ){

        return ( $this->id === $message->getId());

    }
}