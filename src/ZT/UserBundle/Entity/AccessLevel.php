<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ZT\UserBundle\Entity\AccessLevel
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AccessLevel
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $access
     *
     * @ORM\Column(name="access_level", type="string", length=255)
     */
    private $access_level;

        /**
     * @var string $access
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;


    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $isActive;

  /**
     * @Assert\Type(type="ZT\UserBundle\Entity\Goal")
     * @ORM\OneToMany(targetEntity="Goal", mappedBy="access_level")
     */


    protected $goals;

  /**
     * @Assert\Type(type="ZT\UserBundle\Entity\Post")
     * @ORM\OneToMany(targetEntity="Post", mappedBy="access_level")
     */


    protected $posts;

    public function __construct(){

        $this->goals = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->isActive = true;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set access_level
     *
     * @param string $accessLevel
     * @return AccessLevel
     */
    public function setAccessLevel($accessLevel)
    {
        $this->access_level = $accessLevel;
    
        return $this;
    }

    /**
     * Get access_level
     *
     * @return string 
     */
    public function getAccessLevel()
    {
        return $this->access_level;
    }

       /**
     * Set access_level
     *
     * @param string $accessLevel
     * @return AccessLevel
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get access_level
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }
    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return AccessLevel
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add goals
     *
     * @param \ZT\UserBundle\Entity\Goal $goals
     * @return AccessLevel
     */
    public function addGoal(\ZT\UserBundle\Entity\Goal $goals)
    {
        $this->goals[] = $goals;
    
        return $this;
    }

    /**
     * Remove goals
     *
     * @param \ZT\UserBundle\Entity\Goal $goals
     */
    public function removeGoal(\ZT\UserBundle\Entity\Goal $goals)
    {
        $this->goals->removeElement($goals);
    }

    /**
     * Get goals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * Add posts
     *
     * @param \ZT\UserBundle\Entity\Post $posts
     * @return AccessLevel
     */
    public function addPost(\ZT\UserBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;
    
        return $this;
    }

    /**
     * Remove posts
     *
     * @param \ZT\UserBundle\Entity\Post $posts
     */
    public function removePost(\ZT\UserBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }
}