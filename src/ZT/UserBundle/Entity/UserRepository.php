<?php
namespace ZT\UserBundle\Entity;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserRepository extends EntityRepository
implements UserProviderInterface
{
   public function loadUserByUsername($email) {
     return $this->getEntityManager()
         ->createQuery('SELECT u FROM
         ZTUserBundle:User u
         WHERE u.email = :email')
         ->setParameters(array(
                       'email' => $email
                        ))
         ->getOneOrNullResult();
   }
   public function refreshUser(UserInterface $user) {
        return $this->loadUserByUsername($user->getEmail());
   }
   public function supportsClass($class) {
        return $class === 'ZT\UserBundle\Entity\User';
   }
//
//   public function getFriends( User $user){
//
//       $em = $this->getEntityManager();
//
//       $query = $em->createQuery('SELECT u FROM ZTUserBundle:User u WHERE :user MEMBER OF u.friends_with')
//           ->setParameters( array('user'=>$user) );
//
//       return $query->getResult();
//
//   }
//
   
    
}