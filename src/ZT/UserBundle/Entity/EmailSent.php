<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ZT\UserBundle\Entity\User;

/**
 * EmailSent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ZT\UserBundle\Entity\EmailSentRepository")
 */
class EmailSent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**

     * @ORM\ManyToOne(targetEntity="User", inversedBy="emails_sent")

     */

    protected $user;


    /**
     *
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     *
     */

    private $content;


    /**
     * @var string $sentOn
     *
     * @ORM\Column(type="datetime")

     */

    protected $sentOn;

    /**
     * Get id
     *
     * @return integer 
     */

    public function __construct(){

        $this->sentOn = new \DateTime();

    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return EmailSent
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set sentOn
     *
     * @param \DateTime $sentOn
     * @return EmailSent
     */
    public function setSentOn($sentOn)
    {
        $this->sentOn = $sentOn;
    
        return $this;
    }

    /**
     * Get sentOn
     *
     * @return \DateTime 
     */
    public function getSentOn()
    {
        return $this->sentOn;
    }

    /**
     * Set user
     *
     * @param \ZT\UserBundle\Entity\User $user
     * @return EmailSent
     */
    public function setUser(\ZT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \ZT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}