<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ZT\UserBundle\Entity\GoalCategory
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class GoalCategory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $goal_category
     *
     * @ORM\Column(name="goal_category", type="string", length=255)
     */
    private $goal_category;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $isActive;

     /**
     * @Assert\Type(type="ZT\UserBundle\Entity\Goal")
     * @ORM\OneToMany(targetEntity="Goal", mappedBy="goal_category")
     */


    protected $goals;

    public function __construct(){

        $this->goals = new ArrayCollection();
        $this->isActive = true;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set goal_category
     *
     * @param string $goalCategory
     * @return GoalCategory
     */
    public function setGoalCategory($goalCategory)
    {
        $this->goal_category = $goalCategory;
    
        return $this;
    }

    /**
     * Get goal_category
     *
     * @return string 
     */
    public function getGoalCategory()
    {
        return $this->goal_category;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return GoalCategory
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add goals
     *
     * @param \ZT\UserBundle\Entity\Goal $goals
     * @return GoalCategory
     */
    public function addGoal(\ZT\UserBundle\Entity\Goal $goals)
    {
        $this->goals[] = $goals;
    
        return $this;
    }

    /**
     * Remove goals
     *
     * @param \ZT\UserBundle\Entity\Goal $goals
     */
    public function removeGoal(\ZT\UserBundle\Entity\Goal $goals)
    {
        $this->goals->removeElement($goals);
    }

    /**
     * Get goals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGoals()
    {
        return $this->goals;
    }
}