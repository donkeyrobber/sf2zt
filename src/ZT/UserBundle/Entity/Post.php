<?php

namespace ZT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ZT\UserBundle\Entity\AccessLevel;
use JMS\Serializer\Annotation as Serialize;
/**
 * GoalPost
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ZT\UserBundle\Entity\PostRepository")
 * @Serialize\ExclusionPolicy("all")  
 */
class Post
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO") 
     * @Serialize\Expose() 
     * @Serialize\Groups({"post_list"}) 
     */
    protected $id;

    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\Goal")
     * @ORM\ManyToOne(targetEntity="Goal", inversedBy="posts") 
     */

    protected $goal;

    
    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @Serialize\Expose() 
     * @Serialize\Groups({"post_list"})     
 
     */
     
     protected $supporter;
 
     /**
     * @var \DateTime $created_on
     *
     * @ORM\Column(name="created_on", type="datetime")
     * @Serialize\Expose() 
     * @Serialize\Groups({"post_list"})          
     */
    protected $created_on;                          

    /**
     * @var boolean active
     * 
     * @ORM\Column(name="isActive", type="boolean")          
     */         

    protected $isActive;

    /**
     * @var string $comment
     * @Assert\NotBlank()
     * @ORM\Column(name="comment", type="text") 
      * @Serialize\Expose() 
     * @Serialize\Groups({"post_list"})            
     */
    protected $comment;

     /**
     * @Assert\Type(type="ZT\UserBundle\Entity\AccessLevel")
     * @ORM\ManyToOne(targetEntity="AccessLevel", inversedBy="posts") 
     * @Serialize\Expose() 
     * @Serialize\Groups({"post_list"})
     */

    protected $access_level;
    
    public function __construct(){

        $this->created_on = new \DateTime();
        $this->isActive=true;
        //$this->access_level= new AccessLevel();
        
    }  
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * Set created_on
     *
     * @param \DateTime $createdOn
     * @return GoalPost
     */
    public function setCreatedOn($createdOn)
    {
        $this->created_on = $createdOn;
    
        return $this;
    }

    /**
     * Get created_on
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return GoalPost
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set goal
     *
     * @param \ZT\UserBundle\Entity\Goal $goal
     * @return GoalPost
     */
    public function setGoal(\ZT\UserBundle\Entity\Goal $goal = null)
    {
        $this->goal = $goal;
    
        return $this;
    }

    /**
     * Get goal
     *
     * @return \ZT\UserBundle\Entity\Goal 
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set supporter
     *
     * @param \ZT\UserBundle\Entity\User $supporter
     * @return GoalPost
     */
    public function setSupporter(\ZT\UserBundle\Entity\User $supporter = null)
    {
        $this->supporter = $supporter;
    
        return $this;
    }

    /**
     * Get supporter
     *
     * @return \ZT\UserBundle\Entity\User 
     */
    public function getSupporter()
    {
        return $this->supporter;
    }

    /**
     * Set post
     *
     * @param string $post
     * @return GoalPost
     */
    public function setComment($post)
    {
        $this->comment = $post;
    
        return $this;
    }

    /**
     * Get post
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set post_access
     *
     * @param \ZT\UserBundle\Entity\PostAccess $postAccess
     * @return GoalPost
     */
    public function setPostAccess(\ZT\UserBundle\Entity\AccessLevel $postAccess = null)
    {
        $this->access_level = $postAccess;
    
        return $this;
    }

    /**
     * Get post_access
     *
     * @return \ZT\UserBundle\Entity\PostAccess 
     */
    public function getPostAccess()
    {
        return $this->post_access;
    }

    /**
     * Set access_level
     *
     * @param \ZT\UserBundle\Entity\AccessLevel $accessLevel
     * @return GoalPost
     */
    public function setAccessLevel(\ZT\UserBundle\Entity\AccessLevel $accessLevel = null)
    {
        $this->access_level = $accessLevel;
    
        return $this;
    }

    /**
     * Get access_level
     *
     * @return \ZT\UserBundle\Entity\AccessLevel 
     */
    public function getAccessLevel()
    {
        return $this->access_level;
    }
}