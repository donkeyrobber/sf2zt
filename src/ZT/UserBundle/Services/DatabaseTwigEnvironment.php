<?php

namespace ZT\UserBundle\Services;
/**
 * Created by JetBrains PhpStorm.
 * User: robm
 * Date: 30/03/13
 * Time: 20:47
 * To change this template use File | Settings | File Templates.
 */

class RouterTwigEnvironment extends \Twig_Environment
{

    protected $container;

    public function __construct(ContainerInterface $container ){

        $this->container = $container;

        $loader = $this->container->get('twig_db_loader');

        parent::__construct($loader, array('debug' => true));

        $this->addExtension()

    }

}