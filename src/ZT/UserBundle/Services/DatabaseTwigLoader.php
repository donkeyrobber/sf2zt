<?php

namespace ZT\UserBundle\Services;
/**
 * Created by JetBrains PhpStorm.
 * User: robm
 * Date: 30/03/13
 * Time: 20:47
 * To change this template use File | Settings | File Templates.
 */
use Doctrine\ORM\EntityManager;


class DatabaseTwigLoader implements \Twig_LoaderInterface, \Twig_ExistsLoaderInterface
{

    protected $em;
    const TEMPLATE_ENTITY = 'ZTUserBundle:EmailContent';
    protected $template_repo;

    public function __construct( EntityManager $em){

        $this->em = $em;

        $this->template_repo = $this->em->getRepository(self::TEMPLATE_ENTITY);

    }

    public function getSource($name){

        $template = $this->template_repo->findOneBy(array('tag'=> $name) );

        if(is_null($template)){

            throw new \Twig_Error_Loader(sprintf('Template "%s" does not exist.' . $name. 'in '.__METHOD__ , $name));

        }

        return $template->getBody();

    }

    public function exists($name){

        return ($template = $this->template_repo->findOneBy(array('tag'=> $name) ));

    }

    public function getCacheKey($name){

        return $name;

    }

    public function isFresh($name, $time){

        $template = $this->template_repo->findOneBy(array('tag'=> $name) );

        if(is_null($template)){

            throw new \Twig_Error_Loader(sprintf('Template "%s" does not exist.' . $name. 'in '.__METHOD__, $name));

        }

        return $template->getLastModified() <= $time;

    }

}