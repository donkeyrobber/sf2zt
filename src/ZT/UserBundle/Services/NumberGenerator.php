<?php

namespace ZT\UserBundle\Services;

use ZT\UserBundle\Entity\Image;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NumberGenerator extends ContainerAware{

    private $image_size="small";
    
    private $image_path = "/img/numbers";
        
    private $web_dir;
    
    private $image_type = "png";
    
    public function __construct(ContainerInterface $container){
        
        $this->setContainer($container);
        
        $this->web_dir = $this->container->get('kernel')->getRootDir() ."/../web";
                    
        if( ! is_dir($this->web_dir . $this->image_path)){
        
            throw new \Exception('image dir '.$this->image_path.' not found');        
        
        }
    
    }

    public function setSize($size){
    
        $this->image_size=$size;
    
    }
        
    public function setType($image_type){
    
        $this->image_type=$image_type;
    
    }
    public function getImage($number){
    
        if( ! is_integer($number) ){
        
            $number=(int) $number;
        
        }
        
        $image_path = $this->image_path . "/" . $this->image_size;
                           
        if( ! is_dir($this->web_dir . $image_path)){
        
            throw new \Exception('image dir'.$image_path.' not found');        
        
        }        
        
        $number_str = (string) $number;
      
        
        $image_array=array();
        
        for($i=0;$i<strlen($number_str); $i++){
        
            $img = $image_path . "/" . $number_str[$i] . ".".$this->image_type;
        
            if( ! file_exists($this->web_dir . $img)){
            
                throw new \Exception('image '.$img.' not found');                 
            
            }
            
            $image_array[]=$img; 
        
        }
        
        return $image_array;
    
    }

    public function getNumber($number){

        if( ! is_integer($number) ){

            $number=(int) $number;

        }

        $number_str = (string) $number;

        $image_array=array();

        for($i=0;$i<strlen($number_str); $i++){

            $number_array[]=$number_str[$i];

        }

        return $number_array;

    }

}
