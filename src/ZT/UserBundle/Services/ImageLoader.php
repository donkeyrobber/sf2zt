<?php

namespace ZT\UserBundle\Services;

use ZT\UserBundle\Entity\Image;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImageLoader extends ContainerAware{

    private $default_image;
    
    private $web_dir;
    
    private $fetcher;
    
    public function __construct(ContainerInterface $container){
        
        $this->setContainer($container);
        
        $this->web_dir = $this->container->get('kernel')->getRootDir() ."/../web";
                    
    }

    public function setFetcher(DefaultImageFetcherInterface $fetcher){
    
        $this->fetcher = $fetcher;
    
    }

    public function __call($method, $args){
    
        if(  method_exists( $this,$method)){
        
            return call_user_func_array(array($this,$method) , $args );
        
        }else{
        
            if( ! is_null($args[0]) && file_exists($this->web_dir."/".$args[0]) ){
        
                $image_path = $args[0];
        
            }else{
        
                $image_path = call_user_func_array(array($this->fetcher, $method) , $args );        
            }
        
            $obj = new Image();
            
            $obj->setPath($image_path);
        
            return $obj; 
        
        }      
          
        
    }

}
