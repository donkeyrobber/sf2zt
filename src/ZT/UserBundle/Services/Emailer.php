<?php

namespace ZT\UserBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use ZT\UserBundle\Entity\EmailSent;
use Doctrine\ORM\EntityManager;
use ZT\UserBundle\Entity\User;


class Emailer{

    private $mailer;
    private $twig;
    private $em;
    private $message;
    private $rendered;


    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, EntityManager $em ){

        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->em=$em;

    }

    public function setTemplate($tag, $data ){

        if( ! is_string($tag)){

            throw new Exception(__METHOD__ . ' expects parameter 1 to be a string. ' . gettype($tag). ' given');

        }


        $email_content_repo = $this->em->getRepository('ZTUserBundle:EmailContent');

        $email_content = $email_content_repo->findOneBy(array('tag'=>$tag));

        if( is_null($email_content)){

            throw new \Exception('could not find email template: '.$tag);

        }


        try{
            $this->rendered = $this->twig->render($tag,array('data'=> $data));
        }catch(\Exception $e){

            throw new \Exception("couldn't render template " . $tag . ": ". $e->getMessage() );

        }

        $this->message = \Swift_Message::newInstance()
            ->setSubject($email_content->getSubject())
            ->setFrom($email_content->getFrom())
            ->setBody($this->rendered);

        $content_type = $this->message->getHeaders()->get('Content-Type');

        $content_type->setValue($email_content->getContentType());

    }

    public function send(User $to){


        if( get_class($this->message) != 'Swift_Message'){

            $error = __CLASS__.': message is type '. get_class($this->message);
            throw new Exception($error);


        }

        $this->message->setTo($to->getEmail());

        $this->mailer->send($this->message);

        $email_sent = new EmailSent();

        $email_sent->setUser($to);

        $email_sent->setContent($this->rendered);

        $this->em->persist($email_sent);

        $this->em->flush();

    }

}