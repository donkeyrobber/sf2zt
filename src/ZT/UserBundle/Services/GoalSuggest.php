<?php

namespace ZT\UserBundle\Services;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

class GoalSuggest{

    private $client;
    private $index;
    private $type;
    private $user;
    private $em;
    
    public function __construct(SecurityContext $context, EntityManager $em){
    
        $this->client = new \Elastica_Client(); 
        $this->index = $this->client->getIndex('bookmarks');
        $this->type = $this->index->getType('goal');
        $this->user = $context->getToken()->getUser(); 
        $this->em = $em; 
    
    }

    public function getFuzzyLikeThis($goal){
                
        $flt = new \Elastica_Query_FuzzyLikeThis();
                
        $like_text = $goal->getTitle() . " " .$goal->getDescription(); 
                
        $flt->setLikeText($like_text);
                
        $flt->addFields(array('title','description'));
                
        $search = $this->type->search($flt, array('limit'=> 20 ) );
        
       
        
        $goal_repo = $this->em->getRepository('ZTUserBundle:Goal');

        $access_level_repo = $this->em->getRepository('ZTUserBundle:AccessLevel');
        
        $public_access = $access_level_repo->findOneBy(array('access_level'=>'Public'));
         
        $suggestions = array();
        
        if(count($search) > 0 ){
        
            foreach($search as $result){
        
                $goal = $goal_repo->findOneById($result->getId() );
          
            
                if( ! is_null($goal)){
            
                    if( ! $goal->getUser()->equals($this->user) &&
                        $goal->getAccessLevel() === $public_access
                    ){
            
                        $suggestions[]= $goal;
            
                    }
                
                }
                
                if(count($suggestions)==5){
            
                    break;
            
                }     
        
            }
        
        }
        
        return $suggestions;  
    
    }

}