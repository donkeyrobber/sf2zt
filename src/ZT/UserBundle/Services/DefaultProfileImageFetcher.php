<?php

namespace ZT\UserBundle\Services;

use ZT\UserBundle\Entity\Image;
use Doctrine\ORM\EntityManager;


class DefaultProfileImageFetcher implements DefaultImageFetcherInterface{

    public function getDisplayImage(){
    
        return  '/img/default_profile_image.png';
    
    }      

}
