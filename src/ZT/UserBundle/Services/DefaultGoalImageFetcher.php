<?php

namespace ZT\UserBundle\Services;

use ZT\UserBundle\Entity\Image;
use Doctrine\ORM\EntityManager;


class DefaultGoalImageFetcher implements DefaultImageFetcherInterface{

    private $default_images = array(
    
        'health' =>'/img/health.png',
        'wealth' =>'/img/wealth.png',
        'leisure' =>'/img/leisure.png',    
    );

    public function getDisplayImage($image, $goal_category){
    
        if(in_array(strtolower($goal_category->getGoalCategory() ), array_keys($this->default_images) )){
        
            return  $this->default_images[strtolower($goal_category->getGoalCategory() )];        
     
        }
    

    
    }      

}
