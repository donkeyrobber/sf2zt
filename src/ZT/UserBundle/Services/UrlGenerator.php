<?php

namespace ZT\UserBundle\Services;
/**
 * Created by JetBrains PhpStorm.
 * User: robm
 * Date: 01/04/13
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */


use Symfony\Component\Routing\Router;
use ZT\UserBundle\Services\RequestContext;
use Monolog\Logger;

class UrlGenerator extends \Symfony\Component\Routing\Generator\UrlGenerator
{
    public function __construct(Router $router, RequestContext $context, Logger $logger){

        parent::__construct($router->getRouteCollection(), $context, $logger);

    }
}
