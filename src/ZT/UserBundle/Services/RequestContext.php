<?php

namespace ZT\UserBundle\Services;
/**
 * Created by JetBrains PhpStorm.
 * User: robm
 * Date: 16/05/13
 * Time: 22:33
 * To change this template use File | Settings | File Templates.
 */
class RequestContext extends \Symfony\Component\Routing\RequestContext
{

    public function __construct(){

        parent::__construct('','GET', $_SERVER['HTTP_HOST']);

    }

}
