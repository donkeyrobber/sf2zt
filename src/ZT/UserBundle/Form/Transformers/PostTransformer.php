<?php

namespace ZT\UserBundle\Form\Transformers;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use ZT\UserBundle\Entity\Post;

class PostTransformer implements DataTransformerInterface
{

    private $om;
    
    public function __construct(ObjectManager $om){
    
        $this->om = $om;
    
    }
        
    public function transform($post)
    {            
        if (null === $pPost) {
            return "";
        }elseif (is_integer($post)){
        
            return $post;
        
        }

        return $post->getId();
    }
    
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $post = $this->om
            ->getRepository('ZTUserBundle:Post')
            ->findOneBy(array('id' => $id));

        if (null === $id) {
            throw new TransformationFailedException(sprintf(
                'A post with number "%s" does not exist!',
                $id
            ));
        }

        return $post;
    }
    
}