<?php

namespace ZT\UserBundle\Form\Transformers;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use ZT\UserBundle\Entity\Goal;

class GoalTransformer implements DataTransformerInterface
{

    private $om;
    
    public function __construct(ObjectManager $om){
    
        $this->om = $om;
    
    }
        
    public function transform($goal)
    {
        if (null === $goal) {
            return "";
        }

        return $goal->getId();
    }
    
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $goal = $this->om
            ->getRepository('ZTUserBundle:Goal')
            ->findOneBy(array('id' => $id));

        if (null === $id) {
            throw new TransformationFailedException(sprintf(
                'A goal with number "%s" does not exist!',
                $id
            ));
        }

        return $goal;
    }
    
}