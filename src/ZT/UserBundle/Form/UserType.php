<?php

namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UserType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
    
        $builder->add('first_name','text')
            ->add('last_name','text')
            ->add('username','text')
            ->add('email','text')
            ->add('password','repeated', array(
                    'first_name'=>'password',
                    'second_name'=>'confirm_password',
                    'type'=>'password'
                ))
            ->add('location', 'entity' ,array('class'=>'ZTUserBundle:Location','property'=>'location',))
            ->add('date_of_birth','date')
            ->add('gender','choice',array(
                'choices' => array(
                    'M'=>'Male',
                    'F'=>'Female'
                    ), 
                'required'=>true,
                )
               );

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ZT\UserBundle\Entity\User'
        ));
    }



    public function getName(){

        return 'user';

    }

}
