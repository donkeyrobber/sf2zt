<?php

namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UserUpdateType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
    
        $builder->add('first_name','text',array('required' => false))
            ->add('last_name','text',array('required' => false))
            ->add('username','text',array('required' => false))
            ->add('email','text',array('required' => true))
        //    ->add('password','repeated', array(
        //            'first_name'=>'password',
        //            'second_name'=>'confirm_password',
        //            'type'=>'password',
        //            'required' => false
        //        ))
            ->add('location', 'entity' ,array('class'=>'ZTUserBundle:Location','property'=>'location','required' => false))
            ->add('date_of_birth','date',array('attr'=>array('id'=>'date_of_birth'), 
                                                    'widget'=>'single_text',
                                                    'format'=>'dd-MM-yyyy'))
            ->add('gender','choice',array(
                'choices' => array(
                    'M'=>'Male',
                    'F'=>'Female'
                    ), 
                'required'=>true,
                )
               );

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ZT\UserBundle\Entity\User'
        ));
    }



    public function getName(){

        return 'user';

    }

}
