<?php

namespace ZT\UserBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
/**
 * Created by JetBrains PhpStorm.
 * User: robm
 * Date: 11/05/13
 * Time: 21:51
 * To change this template use File | Settings | File Templates.
 */
class PasswordRequestType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('email','text');

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ZT\UserBundle\Entity\User'
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'validation_groups' => array('passwordrequest')
        );
    }

    public function getName(){

        return "PasswordRequest";

    }
}
