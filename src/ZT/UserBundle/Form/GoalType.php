<?php

namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class GoalType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
    
        $builder->add('title','text',array('label'=>'What would you like to achieve?'))
            ->add('description','textarea',array('label'=>'How are you going to do this?'))
            ->add('access_level', 'entity' ,array(
                    'label'=>'Who should be able to see this goal?',
                    'class'=>'ZTUserBundle:AccessLevel',
                    'property'=>'label',
                    'expanded' =>true,
                    'multiple' =>false,
                    'required'=> true,))
            ->add('image', new ImageType(),array('label'=>'You can add an image to your goal'))
            ->add('goal_category','entity',array(
                    'class'=>'ZTUserBundle:GoalCategory',
                    'property'=>'goal_category',
                    'expanded' =>false,
                    'multiple' =>false,
                    'label'=>'Which of these options best describes the nature of your goal?'))
            ->add('timeframe','text',array('label'=>'In how many days do you plan to reach your goal?'));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ZT\UserBundle\Entity\Goal'
        ));
    }
    public function getDefaultOptions(array $options)
    {
        return array(
            'validation_groups' => array('goal_save_edit')
        );
    }

    public function getName(){

        return 'goal';

    }

}
