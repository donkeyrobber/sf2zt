<?php
/**
 * Created by JetBrains PhpStorm.
 * User: robm
 * Date: 07/04/13
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */


namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Security\Core\SecurityContext;
class MessageType extends AbstractType
{

    private $security_context;

    public function __construct(SecurityContext $security_context){

        $this->security_context = $security_context;

    }

    public function buildForm(FormBuilderInterface $builder, array $options){

        $my_user = $this->security_context->getToken()->getUser();

        $builder->add('to', 'entity',array(
            'class'=>'ZTUserBundle:User',
            'property'=>'identifier',
            'expanded' =>false,
            'multiple' =>false,
            'label'=>'To',
            'query_builder'=>function(\Doctrine\ORM\EntityRepository $er) use ($my_user){

                return $er->createQueryBuilder('u')->where(':user MEMBER OF u.friends_with')
                    ->setParameter('user',$my_user);

            }))
            ->add('subject','text',array('label'=>'Subject'))
            ->add('body', 'textarea', array('label'=>'Message'));

    }





    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ZT\UserBundle\Entity\Message'
        ));
    }


    public function getName(){

        return 'message';

    }


}
