<?php

namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UserRegType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
    
        $builder->add('date_of_birth','date',array('attr'=>array('id'=>'date_of_birth'), 
                                                    'widget'=>'single_text',
                                                    'format'=>'dd-MM-yyyy'))
            ->add('email','text')
            ->add('password','repeated', array(
                    'first_name'=>'password',
                    'second_name'=>'confirm_password',
                    'type'=>'password'
                ));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ZT\UserBundle\Entity\User'
        ));
    }



    public function getName(){

        return 'user';

    }

}
