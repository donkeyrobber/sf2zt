<?php

namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use ZT\UserBundle\Form\Transformers\GoalTransformer;

class PostType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
    
        $entityManager = $options['em'];
        $transformer = new GoalTransformer($entityManager);
        
        $builder->add(
            $builder->create('goal', 'hidden')
                ->addModelTransformer($transformer)
        )
                ->add('comment','text');


    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ZT\UserBundle\Entity\Post'
        ));
        
        $resolver->setRequired(array(
            'em',
        ));

        $resolver->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
        ));        
        
    }



    public function getName(){

        return 'Post';

    }

}