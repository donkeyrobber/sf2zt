<?php

namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SearchType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
    
        $builder->add('searchtext','text',array('label'=>'Search')) ;

    }

//     public function setDefaultOptions(OptionsResolverInterface $resolver)
//     {
//         $resolver->setDefaults(array(
//             'data_class' => 'ZT\UserBundle\Entity\Goal'
//         ));
//     }
//     public function getDefaultOptions(array $options)
//     {
//         return array(
//             'validation_groups' => array('goal_save_edit')
//         );
//     }

    public function getName(){

        return 'search';

    }

}
