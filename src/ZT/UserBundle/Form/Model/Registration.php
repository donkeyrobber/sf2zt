<?php

namespace ZT\UserBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use ZT\UserBundle\Entity\User;

class Registration{

    /**
     * @Assert\Type(type="ZT\UserBundle\Entity\User")
     */

    protected $user;

    /**
     *@Assert\NotBlank()
     *@Assert\True()
     */

    protected $termsAccepted;

    public function setUser(User $user){

        $this->user = $user;

    }

    public function getUser(){

        return $this->user;

    }

    public function setTermsAccepted($termsAccepted){

        $this->termsAccepted = (boolean) $termsAccepted;

    }

    public function getTermsAccepted(){

        return $this->termsAccepted;

    }

}
