<?php

namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('user', new UserRegType())
            ->add('terms_accepted','checkbox',array('property_path'=>'termsAccepted'));

    }


    public function getDefaultOptions(array $options)
    {
        return array(
            'validation_groups' => array('registration')
        );
    }


    public function getName(){

        return 'registration';

    }    

}

