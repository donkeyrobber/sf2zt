<?php

namespace ZT\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use ZT\UserBundle\Form\UserType;
use ZT\UserBundle\Form\ImageType;

class ProfileUpdateType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('introduction', 'text',array('required'=>false))
                ->add('user', new UserUpdateType())
                ->add('image', new ImageType());

    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'validation_groups' => array('profile')
        );
    }

    public function getName(){

        return 'profile';

    }    

}

