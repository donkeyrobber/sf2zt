<?php
/**
 * Created by JetBrains PhpStorm.
 * User: robm
 * Date: 07/04/13
 * Time: 14:52
 * To change this template use File | Settings | File Templates.
 */

namespace ZT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ZT\UserBundle\Entity\Message;
use ZT\UserBundle\Entity\Conversation;
use ZT\UserBundle\Form\MessageType;

class MessageController extends  Controller
{


    /**
     * @Route("/message/create/{reply_id}", defaults={"reply_id": null}, name="message_create")
     * @Template("ZTUserBundle:Message:create.html.twig")
     */
    public function createAction(Request $request, $reply_id)
    {

        $my_user = $this->getUser();

        $message = new Message();

        if( is_null($reply_id ) ){

            $conversation = new Conversation();

        }else{

            $message_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Message');


            try{

                $reply_to = $message_repo->find($reply_id);

                $conversation = $reply_to->getConversation();

                $message->setTo($reply_to->getFrom());

                $message->setSubject($reply_to->getSubject());

            }catch(\Exception $e){

                $conversation = new Conversation();

                $this->get('logger')->err('failed to retrieve conversation entity '. $reply_id . ': ' . $e->getMessage() );

                throw $e;
            }

        }



        $message->setFrom($my_user);

        $message->setConversation($conversation);

        $form = $this->createForm($this->get('message_type') , $message);

        if( $request->getMethod() == 'PUT' ){

            $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');

            $to = $request->request->get('to');

            try{

                $user_to = $user_repo->find($to);

            }catch(\Exception $e){

                throw new \Exception("Can't find user: ". $e->getMessage() );

            }

            if( is_null($user_to) ){

                throw new \Exception('user '. $to .' not found');

            }

            $message->setTo($user_to);

            $subject = $request->request->get('subject');

            $message->setSubject($subject);

            $body = $request->request->get('body');

            $message->setBody($body);

            $em = $this->getDoctrine()->getEntityManager();

            $em->persist($message);

            $em->flush();

            return new Response("Success", 200);
        }

        return array(
            'title'=>'Send Private Message',
            'form'=>$form->createView(),
            'reply_id'=>$reply_id);

    }
    /**
     * @Route("/message/view/{id}", name="message_view", options={"expose"=true})
     * @Template("ZTUserBundle:Message:view.html.twig")
     */
    public function viewAction(Request $request, $id)
    {

        $my_user = $this->getUser();



            $message_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Message');

            $message = $message_repo->find($id);

            try{

            }catch(\Exception $e){

                $this->get('logger')->err('could not find message '. $id);


            }




        if( is_null( $message )){

                throw new \Exception('message '. $id . 'could not be found');

            }


        if( count($message->getConversation()->getMessages()) > 0){

            foreach($message->getConversation()->getMessages() as $m){

                if($m->equals($message) ){

                    continue;

                }

                $history[] = $m;

            }
        }

            return array(
                'title'=>'View Private Message',
                'message'=>$message,
                'history'=>isset($history) ? $history : array(),
                );




    }
    /**
     * @Route("/message/edit/{id}", name="message_edit")
     * @Template("ZTUserBundle:Message:edit.html.twig")
     */
    public function editAction(Request $request, $id)
    {


    }
    /**
     * @Route("/message/send/{id}", name="message_send")
     */
    public function sendAction(Request $request, $id)
    {


    }
    /**
     * @Route("/message/delete/{id}", name="message_delete")
     */
    public function deleteAction(Request $request, $id)
    {


    }
}