<?php

namespace ZT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ZT\UserBundle\Entity\Goal;
use ZT\UserBundle\Form\GoalType;
use ZT\UserBundle\Services\DefaultProfileImageFetcher;
use ZT\UserBundle\Services\DefaultGoalImageFetcher;


class NetworkController extends Controller
{

    /**
     * @Route("/network/{id}/supporting", defaults={"id"=null}, name="network_supporting")
     * @Template("ZTUserBundle:Network:supporting.html.twig")
     */
    public function supportedAction(Request $request, $id)
    {  
        $NG = $this->get('number_generator');

        $my_user = $this->getUser();
                
        $user = $my_user;
        
        if( ! is_null( $id ) ){
        
            $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');
      
            $user = $user_repo->findOneBy(array('username'=>$id));
      
            if( is_null($user) ){
      
                $user = $user_repo->findOneBy(array('id'=>$id));          
      
            }
 
            if( is_null($user) ){
        
                throw new \Exception('user' . $id. ' can not be found');
        
            }

        }
        
        $goal_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Goal');
        
        $supported_goals = $goal_repo->getSupportedGoals($user);            

        return array('user'=>$my_user,
                        'my_user'=>$my_user,
                        'supported_goals' =>$supported_goals,
                        'title'=>'Zimtam network',
                        'NG'=>$NG,);

    

        

    } 

        /**
     * @Route("/network/{id}/friends", defaults={"id"=null}, name="network_friends")
     * @Template("ZTUserBundle:Network:friends.html.twig")
     */
    public function friendsAction(Request $request, $id)
    {  
    
        $NG = $this->get('number_generator');
    
        $my_user = $this->getUser();
                
        $user = $my_user;
        
        if( ! is_null( $id ) ){
        
            $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');
      
            $user = $user_repo->findOneBy(array('username'=>$id));
      
            if( is_null($user) ){
      
                $user = $user_repo->findOneBy(array('id'=>$id));          
      
            }
 
            if( is_null($user) ){
        
                throw new \Exception('user' . $id. ' can not be found');
        
            }

        }


        return array('user'=>$my_user,
                        'my_user'=>$my_user,
                        'title'=>'Zimtam network');

    
    }
    
    /**
     * @Route("/network/friends/add/{id}", name="network_friend_add")
     */
    public function friendAddAction(Request $request, $id)
    {
 
        $my_user = $this->getUser();
        
        $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');
        
        $friend = $user_repo->findOneById($id);
        
        $my_user->addFriendsBy($friend);
        
        $friend->addFriendsBy($my_user);
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $em->persist($my_user);
        
        $em->flush(); 
    
        return $this->redirect($request->headers->get('referer')); 
    
    }  

    
    /**
     * @Route("/network/friends/remove/{id}", name="network_friend_remove")
     */
    public function friendRemoveAction(Request $request, $id)
    {
 
        $my_user = $this->getUser();
                
        $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');
          
        $friend = $user_repo->findOneBy(array('username'=>$id));
      
            if( is_null($friend) ){
      
                $friend = $user_repo->findOneBy(array('id'=>$id));          
      
            }
 
            if( is_null($friend) ){
        
                throw new \Exception('friend' . $id. ' can not be found');
        
            }
         
          
         
        $my_user->removeFriendsBy($friend);
      
        $friend->removeFriendsBy($my_user);
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $em->persist($my_user);
        $em->persist($friend);        
        $em->flush(); 
    
        return $this->redirect($request->headers->get('referer')); 
    
    }  
   
}