<?php

namespace ZT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ZT\UserBundle\Form\SearchType;

class SearchController extends Controller
{



    /**
     * @Route("/search", name="search")
     * @Template("ZTUserBundle:Search:search.html.twig")
     */
    public function searchAction(Request $request)
    {   
        $NG = $this->get('number_generator'); 
                   
        $my_user = $this->getUser();
        
        $user = $my_user;
        
        $searchType = new SearchType();
        
        $form = $this->createForm($searchType);
        
        if($request->getMethod()==='POST'){
        
            $form->bind($request);
        
            $data = $form->getData(); 
 
            $search_term = $data['searchtext'];
 
            $finder = $this->get('foq_elastica.finder.bookmarks.goal');

            $title_query = new \Elastica_Query_Text();
        
            $title_query->setFieldQuery('title',$search_term);

            $results = $finder->find($title_query);

        }


        $nav_menu= array(
        
            array('page' => 'Profile', 'route' => 'profile_edit'),
            array('page' => 'Create Goal', 'route' => 'goal_create'),
            array('page' => 'Log Out', 'route' => 'logout'),        
        
        );
      
        return array('my_user' => $my_user,
                     'user' =>$my_user,
                        'results' => isset($results) ? $results : null  ,
                        'NG'=>$NG,
                        'form' => $form->createView(),
                        'search_term' =>  isset($search_term) ? $search_term : null  ,
                        'nav_menu'=>$nav_menu);

 
   

    }
   

}
