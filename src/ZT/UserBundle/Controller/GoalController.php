<?php

namespace ZT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ZT\UserBundle\Entity\Goal;
use ZT\UserBundle\Form\GoalType;
use ZT\UserBundle\Services\DefaultProfileImageFetcher;
use ZT\UserBundle\Services\DefaultGoalImageFetcher;
use ZT\UserBundle\Entity\Post;
use ZT\UserBundle\Form\PostType;
use ZT\UserBundle\Form\PostControlType;
use Elastica\Client;

class GoalController extends Controller
{

    /**
     * @Route("/goal/create/{access_level}", defaults={"access_level"=null}, name="goal_create")
     * @Template("ZTUserBundle:Goal:create.html.twig")
     */
    public function createAction(Request $request, $access_level)
    {   
        $NG = $this->get('number_generator');  

        $my_user = $this->getUser();
       
        $user = $my_user;

        $goal = new Goal();
        
        $goal->setUser($user);

        $access_level_repo = $this->getDoctrine()->getRepository('ZTUserBundle:AccessLevel');

        $access_level_obj = $access_level_repo->findOneBy(array('access_level'=> ucfirst($access_level) ));

        if( $access_level_obj){

            $goal->setAccessLevel($access_level_obj);

        }

        $goalType= new GoalType();

        $form = $this->createForm($goalType, $goal);

        if( $request->getMethod() == 'POST' ){
          
            $form->bind($request);

            if( $form->isValid()){

                $em = $this->getDoctrine()->getEntityManager();
                
                $goal = $form->getData();

                $image = $goal->getImage();
                          
                if( ! is_null( $image->getPath() )){

                    $image->upload();

                    $em->persist($image);

                }else{

                    $goal->setImage(null);


                }
                $em->persist($goal);


                $em->flush();
//                
                return $this->forward('ZTUserBundle:Goal:suggest', array('goal_id' =>$goal->getId() ) );      
            }  
              
        }


        return array(
                    'title'=>'Create your new goal',
                    'form'=>$form->createView());

    }



    /**
     * @Route("/goal/edit/{goal_id}", name="goal_edit")   
     * @Template("ZTUserBundle:Goal:edit.html.twig")
     */

    public function editAction(Request $request, $goal_id){

        $my_user = $this->getUser();
        $NG = $this->get('number_generator');  
    
        $user = $my_user;    
    
        $goal_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Goal');
         
         $goal = $goal_repo->find($goal_id);
         
         if( ! $goal->getUser()->equals($my_user)){
         
             throw new \Exception("You can't edit this goal!");
         
         }
         

         $goalType= new GoalType();

         $form = $this->createForm($goalType, $goal);

            
        if($request->getMethod() == 'POST' ){
          
            $form->bind($request);
           
            if($form->isValid()){
  
                $em = $this->getDoctrine()->getEntityManager();
                
                $goal = $form->getData();          
                
                          
                    
                $image = $goal->getImage();                
                
                if($request->request->get('delete')){
                       
                       $goal->setIsActive(0);
                }else{

                    if( ! is_null( $image->getPath()) ){

                        $image->upload();

                        $em->persist($image);

                    }
//                    else{
//
//                        $goal->setImage(null);
//
//                    }
                
                }       
                 
                $em->flush();

                return $this->forward('ZTUserBundle:Goal:goalList', array('goal_type' =>$goal->getAccessLevel()->getAccessLevel() ) );      
            }

        }  

        return array('title'=>'Update your goal',
                    'form'=>$form->createView(),
                        'goal'=>$goal);


    }
 
    /**
     * @Route("/goal/view/{goal_id}", name="goal_view", options={"expose"=true})
     * @Template("ZTUserBundle:Goal:view.html.twig")
     */

    public function viewAction(Request $request, $goal_id){
    
         $my_user = $this->getUser();
         
         $goal_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Goal');
         
         $goal = $goal_repo->find($goal_id);  

         if( is_null($goal)){

             throw new \Exception('Goal '. $goal_id. 'not found');

         }

         $user = $goal->getUser(); 
         
                  
         $post_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Post');
                  
         $posts = $post_repo->getActivePosts($goal);
                  
         foreach($posts as $k=>$v){
                           
             $form = $this->createForm( new PostControlType(), $v,array(
    'em' => $this->getDoctrine()->getEntityManager())); 
                                         
             $posts[$k]->form = $form->createView();   
         
         }
                                    
         
         $NG = $this->get('number_generator');
            
         $post = new Post();

         $post->setGoal($goal);

         $post_type= new PostType();

         $form = $this->createForm($post_type, $post,array(
    'em' => $this->getDoctrine()->getEntityManager()));       
         
       

        return array('my_user'=>$my_user,
                        'user'=>$user,
                        'goal'=>$goal,
                        'title' => ($user->equals($my_user)? 'Your' : $user->getIdentifier()."'s") . ' goal',
                        'posts' =>$posts,
                        'form'=>$form->createView());

    }  
    
  /**
     * @Route("/goal/post", name="goal_put",requirements={"_method"="PUT"})
     */

    public function PostPutAction(Request $request){


         if( ! $request->isXmlHttpRequest()){
         
             $request->getSession()->setFlash('error','We are currently unable to save your post. Please try again later');
         
             //$_logger->err('Non Ajax ' . $request->getMethod(). ' request to ZTUserBundle:Goal:PostPut (in '.__CLASS__.' on line '.__LINE__);
         
             $this->redirect($request->headers->get('referer'));
         
         }
  
         $my_user = $this->getUser();  
    
         $post = new Post();
         
         if( ! $request->request->get('goal_id')){
            
             $request->getSession()->setFlash('error','We are currently unable to save your post. Please try again later');
             $_logger->err('missing GET parameter goal_id (in '.__CLASS__.' on line '.__LINE__);
         
         }else{

             $goal_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Goal'); 
         
             $goal = $goal_repo->findOneBy(array('id'=>$request->request->get('goal_id')));
         
             $post->setComment($request->request->get('comment'));

             $access_level_repo = $this->getDoctrine()->getRepository('ZTUserBundle:AccessLevel'); 

             $private = $access_level_repo->findOneBy(array('id'=>2));

             $post->setAccessLevel($private);
  
             $em = $this->getDoctrine()->getEntityManager();

             $post->setGoal($goal);
         
             $post->setSupporter($my_user);
         
             $em->persist($post);

             $em->flush();

             if(!$my_user->equals($post->getGoal()->getUser()) ){

                 $emailer = $this->get('emailer');

                 $emailer->setTemplate('NEW_POST_ON_YOUR_GOAL',$post);

                 $emailer->send($post->getGoal()->getUser());

             }
         
         }
         
         return $this->forward('ZTUserBundle:Goal:view', array('goal_id' =>$goal->getId() ) );
    }    

 /**
     * @Route("/goal/post", name="goal_delete",requirements={"_method"="DELETE"}, options={"expose"=true})
     */

    public function goalPostDelete(Request $request){ 

         if( ! $request->request->get('post_id')){
            
             exit('post not found!');
            
         }

         $my_user = $this->getUser();  
      
         $post_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Post');
         
         $post = $post_repo->findOneById($request->request->get('post_id')); 
         
         $post->setIsActive(0);
                               
         $em = $this->getDoctrine()->getEntityManager();
                  
         $em->persist($post);
          
         $em->flush();         
           
         $goal = $post->getGoal();

        return $this->forward('ZTUserBundle:Goal:view', array('goal_id' =>$goal->getId() ) );    
    
    }    
    
    
    /**
     * @Route("/goal/support/{goal_id}", name="goal_support")
     */

    public function supportAction(Request $request, $goal_id){
    
        $my_user = $this->getUser();
           
        $goal_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Goal');
         
        $goal = $goal_repo->find($goal_id);
        
        if( is_null($goal) ){
        
            throw new \Exception('goal could not be found');
        
        }
        
        $goal->addSupporter($my_user);
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $em->persist($my_user);
        
        $em->flush();

        if(!$my_user->equals($goal->getUser()) ){

            $emailer = $this->get('emailer');

            $emailer->setTemplate('NEW_SUPPORTER_ON_YOUR_GOAL',array('goal'=>$goal,'supporter'=>$my_user));

            $emailer->send($goal->getUser());

        }

        return $this->redirect($request->headers->get('referer') );
    
    
    } 
   
  /**
     * @Route("/goal/unsupport/{goal_id}/{user_id}", defaults={"user_id"=null},  name="goal_unsupport")
     */

    public function unsupportAction(Request $request, $goal_id, $user_id){
    
        $my_user = $this->getUser();
                
        $user = $my_user;
        
        if( ! is_null( $user_id ) ){
        
            $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');
      
            $user = $user_repo->findOneBy(array('username'=>$user_id));
      
            if( is_null($user) ){
      
                $user = $user_repo->findOneBy(array('id'=>$user_id));          
      
            }
 
            if( is_null($user) ){
        
                throw new \Exception('user' . $user_id. ' can not be found');
        
            }

        }
           
        $goal_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Goal');
         
        $goal = $goal_repo->find($goal_id);
        
        if( is_null($goal) ){
        
            throw new \Exception('goal could not be found');
        
        }

        $goal->removeSupporter($user);
        


        $em = $this->getDoctrine()->getEntityManager();
        
        $em->persist($goal);
        //$em->persist($my_user);
        //       $em->persist($user);        
        $em->flush();
        
        return $this->redirect($request->headers->get('referer') );
    
    
    } 
    
    /**
     * @Route("/goal/suggest/{goal_id}", name="goal_suggest", options={"expose"=true})
     * @Template("ZTUserBundle:Goal:suggest.html.twig")
     */
    public function suggestAction(Request $request, $goal_id)
    {   

        $NG = $this->get('number_generator');   

                $goal_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Goal');
            
        
            $goal = $goal_repo->findOneBy(array('id'=>$goal_id));
        
            $GS = $this->get('goal_suggest');
                
            $suggestions = $GS->getFuzzyLikeThis($goal);  

            return array('NG'=>$NG,'title' => 'Other goals that may be of interest', 'suggestions'=>$suggestions);            

    
    } 
    

    /**
     * @Route("/goal/list/{goal_type}/{user_id}", name="goal_list", options={"expose"=true})
     * @Template("ZTUserBundle:Default:new_goal_list.html.twig")
     */

    public function goalListAction(Request $request,$goal_type,$user_id=null){
    
                                                           
        $my_user = $this->getUser();
                
        $user = $my_user;

        if( ! is_null( $user_id ) ){
        
            $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');
      
            $user = $user_repo->findOneBy(array('username'=>$user_id));
      
            if( is_null($user) ){
      
                $user = $user_repo->findOneBy(array('id'=>$user_id));          
      
            }
 
            if( is_null($user) ){
        
                throw new \Exception('user' . $user_id. ' can not be found');
        
            }

        }
        
        $goal_repo = $this->getDoctrine()->getRepository('ZTUserBundle:Goal');

        $NG = $this->get('number_generator'); 
        
        switch(strtolower($goal_type)){
        
            case "public":
            
                $goals = $goal_repo->getUserCurrent($user,'public');
                
            break;
            case "private":
            
                if( ! $user->equals($user) ){
                
                    throw new \Exception("You can not view another users private goals");
                
                }             
                
                $goals = $goal_repo->getUserCurrent($user,'private');
                     
            break;
            case "other":
         
                $goals = $goal_repo->getOtherCurrent($user );           
            
            break;
        
        }
                   
        return array('NG'=>$NG,'type'=>strtolower($goal_type),'goals' => $goals,'user'=>$user,'my_user'=>$my_user);        
      

        


           

    }
    
}
