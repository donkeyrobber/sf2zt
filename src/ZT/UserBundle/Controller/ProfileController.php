<?php

namespace ZT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ZT\UserBundle\Entity\Profile;
use ZT\UserBundle\Form\ProfileUpdateType;
use Symfony\Component\Form\FormError;

class ProfileController extends Controller
{



    /**
     * @Route("/profile/edit", name="profile_edit")
     * @Template("ZTUserBundle:Profile:profile_edit.html.twig")
     */
    public function profileAction(Request $request)
    {   

        $my_user = $this->getUser();
                
        $user = $my_user;

        $profile = $user->getProfile();

        $profileType = new ProfileUpdateType();
         
        $form = $this->createForm( $profileType, $profile,array('cascade_validation'=>true,'validation_groups' => array('profile')));
        
        if( $request->getMethod() == "POST" ){

            $form->bind($request);

            if( $form->isValid()){
    
                $em = $this->getDoctrine()->getEntityManager();  
                
                $profile = $form->getData();
                
		            $image = $profile->getImage();

                //$user = $profile->getUser();

                if( ! is_null( $image->getPath() )){

                    $image->upload();
       
                    $em->persist($image);
                
                }else{

                    $profile->setImage(null);

                }

                   $em->persist($profile);

                    
                    $em->flush();
                    
             return $this->forward('ZTUserBundle:Profile:view');                   
            }
            

     }

        return array(
        'form'=>$form->createView(),
        'title'=>'Update your profile');

    }

    /**
     * @Route("/profile/view/{id}", defaults={"id"=null}, name="profile_view", options={"expose"=true})
     * @Template("ZTUserBundle:Profile:profile_view.html.twig")
     */

    public function viewAction(Request $request, $id=null){

        $my_user = $this->getUser();
               
        $user = $my_user;

        $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');        
         
        if( ! is_null( $id ) ){
        
            $user = $user_repo->findOneBy(array('username'=>$id));

            if( is_null($user) ){
      
                $user = $user_repo->findOneBy(array('id'=>$id));          
      
            }
 
            if( is_null($user) ){
        
                throw new \Exception('user' . $id. ' can not be found');
        
            }

        }


        $NG = $this->get('number_generator');        

        return array('my_user'=>$my_user,
                        'user'=>$user,
                        'NG'=>$NG,);
    }

      /**
     * @Route("/profile/mini/{id}", name="profile_mini")
     * @Template("ZTUserBundle:Profile:mini.html.twig")
     */

    public function miniAction(Request $request, $id){
        
        $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');

        if( ! is_null( $id ) ){
        
            $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');
      
            $user = $user_repo->findOneBy(array('username'=>$id));
      
            if( is_null($user) ){
      
                $user = $user_repo->findOneBy(array('id'=>$id));          
      
            }
 
            if( is_null($user) ){
        
                throw new \Exception('user' . $id. ' can not be found');
        
            }

        }
        
        return array(   'title' =>'User Profile',
                        'user' => $user);
    }


}
