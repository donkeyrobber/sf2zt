<?php

namespace ZT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ZT\UserBundle\Form\Model\Registration;
use ZT\UserBundle\Form\RegistrationType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use ZT\UserBundle\Entity\Profile;
use Symfony\Component\Form\FormError;

class RegistrationController extends Controller implements ContainerAwareInterface
{


    public function setContainer(ContainerInterface $container = null){
        $this->container = $container;
    }

    /**
     * @Route("/register", name="register")
     * @Template("ZTUserBundle:Default:register.html.twig")
     */
    public function registerAction(Request $request)
    {

        $registration = new Registration();

        $registrationType = new RegistrationType();
        
        $form = $this->createForm( $registrationType, $registration,array('cascade_validation'=>true, 'validation_groups'=>'registration'));

        if( $request->getMethod() == 'POST' ){

           $form->bind($this->getRequest() );

            if( $form->isValid() ){

		            $registration = $form->getData();

                $user = $registration->getUser();

                $factory = $this->container->get('security.encoder_factory');

                $encoder = $factory->getEncoder($user);

                $password = $encoder->encodePassword($user->getpassword() ,$user->getSalt());

                $user->setPassword($password);

                $profile = new Profile();

                $profile->setUser($user);
                
                $user->setProfile($profile);
                
                $em = $this->getDoctrine()->getEntityManager();

                $em->persist( $user );     
                    
                $em->flush();
                  
                return $this->redirect($this->generateUrl('login'));                  
                

            }

        }

        return array('form' => $form->createView(),
                      );
    }

}
