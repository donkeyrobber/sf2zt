<?php

namespace ZT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ZT\UserBundle\Services\DefaultProfileImageFetcher;
use ZT\UserBundle\Services\DefaultGoalImageFetcher;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;
use ZT\UserBundle\Entity\Goal;
use ZT\UserBundle\Form\GoalType;

class DefaultController extends Controller
{
    /**
     * @Route("/view/{id}", defaults={"id"=null} , name="profile", options={"expose"=true})
     * @Template("ZTUserBundle:Default:index.html.twig")
     */ 
    public function indexAction($id=null)
    {

        $NG = $this->get('number_generator'); 
        
        $my_user = $this->getUser();
            
        $user = $my_user;
        
        $user_repo = $this->getDoctrine()->getRepository('ZTUserBundle:User');        
        
        if( ! is_null( $id ) ){
        
            $user = $user_repo->findOneBy(array('username'=>$id));
      
            if( is_null($user) ){
      
                $user = $user_repo->findOneBy(array('id'=>$id));          
      
            }
 
            if( is_null($user) ){
        
                throw new \Exception('user' . $id. ' can not be found');
        
            }

        }

        if($my_user->equals($user)){

            $em = $this->getDoctrine()->getEntityManager();

            $message_repo = $em->getRepository('ZTUserBundle:Message');

            $messages = $message_repo->getMessagesAddressedTo($user);

        }

        $goal_repo= $this->getDoctrine()->getRepository('ZTUserBundle:Goal');

        $other_goals=$goal_repo->getOtherCurrent($user);

        return array(
        
            'my_user' => $my_user,
            'user' => $user,
            'NG'=>$NG,
            'messages'=>isset($messages) ? $messages : array(),
            'other_goals'=>$other_goals
        );
    }
    
    /**
     * @Route("/dashboard", name="dashboard")
     * @Template("ZTUserBundle:Default:dashboard.html.twig")
     */
    public function dashboardAction()
    {

        $NG = $this->get('number_generator'); 
        
        $my_user = $this->getUser();
                
        $user = $my_user;

        $post_repo= $this->getDoctrine()->getRepository('ZTUserBundle:Post');

        $new_posts=$post_repo->getNewPostsFromNetwork($user);

        $goal_repo= $this->getDoctrine()->getRepository('ZTUserBundle:Goal');

        $new_goals=$goal_repo->getGoalsFromNetwork($user, 6);

        $other_goals=$goal_repo->getOtherCurrent($user);
//        $other_goals_tmp=$goal_repo->getOtherCurrent($user);
//
//        $other_goals = array();
//
//        for($i=0;$i<(count($other_goals_tmp) / 3 );$i++){
//
//            $other_goals[0][] = array_pop($other_goals_tmp);
//            $other_goals[1][] = array_pop($other_goals_tmp);
//            $other_goals[2][] = array_pop($other_goals_tmp);
//        }

         return array('my_user' => $my_user,
                     'user' => $user,
                        'NG'=>$NG,
                        'new_goals_in_network' => $new_goals,
                        'new_posts_in_network' => $new_posts,
                        'other_goals' => $other_goals
                    );
    }

    /**
     * @Route("/goalfeed/{owner}", name="goal_feed", options={"expose"=true})
     * @Template("ZTUserBundle:Default:dashboard.html.twig")
     */
    public function goalFeedAction($owner)
    {

        $serializer = $this->get('jms_serializer');

        $my_user = $this->getUser();

        $user = $my_user;

        $goal_repo= $this->getDoctrine()->getRepository('ZTUserBundle:Goal');

        switch($owner){

            case "me":

                $goals = array_merge($goal_repo->getUserCurrent($user,'public'),
                $goal_repo->getUserCurrent($user,'private'));

            break;
            case "network":

                $goals = $goal_repo->getGoalsFromNetwork($user);

            break;
            default:

                throw new \Exception($owner. ' is not a valid parameter in '.__METHOD__);

            break;
        }

        $json_goal_feed = $serializer->serialize($goals,'json',SerializationContext::create()->setGroups(array('goal_feed')));

        return new Response($json_goal_feed,200,array('content-type:application/json'));

    }


    /**
     * @Route("/calendarday/{d}", name="calendar_day", options={"expose"=true})
     * @Template("ZTUserBundle:Default:calendarday.html.twig")
     */
    public function calendardayAction($d)
    {

        $my_user = $this->getUser();

        $user = $my_user;

        $date = new \DateTime(urldecode($d));

        $today = new \DateTime();

        $timeframe = $today->diff($date)->days;

        $goal_repo= $this->getDoctrine()->getRepository('ZTUserBundle:Goal');

        $user_goals = $goal_repo->getUserGoalsByDate($my_user,$date);

        $network_goals = $goal_repo->getNetworkGoalsByDate($my_user,$date);

        $goal = new Goal();

        $goal->setUser($user);

        $goal->setTimeframe($timeframe);

        $goalType= new GoalType();

        $form = $this->createForm($goalType, $goal);

        return array('title' => 'date is '.$date->format('d/m/Y'),
             'my_user' => $my_user,
             'user' => $user,
             'user_goals' => $user_goals,
             'network_goals' => $network_goals,
             'timeframe' => $timeframe,
             'form'=>$form->createView()
        );

    }
}
