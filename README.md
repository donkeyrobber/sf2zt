Zimtam Zero

Virtualhost

<VirtualHost *:80>
    ServerName local.zimtam
    DocumentRoot /var/www/sf2zt/web
    
    <Directory /var/www/sf2zt>
    
        Options Indexes
        AllowOverride All
        Order Deny, Allow
        Allow from all
        
   </Directory>
   
</Virtual>     

