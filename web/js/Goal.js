           function Goal(data){
           
           
           
               return {
               
                   'user' : data.user,
                   
                   'profile' : data.profile,
                   
                   'title' : data.title,
                   
                   'image' : data.image.path,
                   
                   'id' : data.id,
                   
                   'getDaysRemaining' : function(){
                   
                       var timeframe = data.timeframe * 86400000; 
                   
                       var created_on = new Date(data.created_on);
                       
                       var end_date = new Date( created_on.getTime() + timeframe );
                       
                       var now = new Date();
                       
                       
                       
                       var time_remaining = end_date.getTime() - now.getTime();
                       
                       var days_remaining = time_remaining / 86400000;
                      
                       if(days_remaining <0){
                       
                           days_remaining =0;
                       
                       }
                      
                       return Math.round(days_remaining);
                       
                   
                   }                   
                   
               
               }    
           
           
           }