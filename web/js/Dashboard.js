(function($){

    $('#calendar').fullCalendar({
                                    'header':
                                        {'left':'prev',
                                        'center':'title',
                                        'right':'next'
                                        },
        'eventSources':[

                                        {'url': Routing.generate( 'goal_feed',{'owner':'me'}),
                                            'color': 'red'

                                        },
                                        {'url': Routing.generate( 'goal_feed', {'owner':'network'}),
                                            'color': 'green'

                                        }
    ],
                                    'dayClick': function(date,e)
                                    {

                                        var url = Routing.generate('calendar_day',{'d':date.toDateString() });

                                        $.ztcolorbox({'href':url});

                                    }

                                });

    var add_goal_callback = {

        init : function(){

            $('#add_goal_iframe').on('load',add_goal_callback.load);
            $('.view_goal').ztcolorbox(view_goal_callback.init);

        },
        load : function(){

            var html  = $('#add_goal_iframe').contents().find('#cb_wrapper').html();

            $('#cb_wrapper').html(html);

            $('#add_goal_container .cb').ztcolorbox();

        }

    }

    var view_friends_callback = {

        init : function(){

            $('.view_profile').ztcolorbox();

        }

    }

    $('a.view_goal').ztcolorbox(view_goal_callback.init);
    $('div.view_goal').each(function(){
        $(this).ztcolorbox(
        {'href': Routing.generate( 'goal_view',{'goal_id': $(this).find('input#goal_id').val() }),
            'onComplete':view_goal_callback.init}); });

    $('.view_goal').ztcolorbox( view_goal_callback.init );
    $('.fc-widget-content').ztcolorbox(add_goal_callback.init);
    $('.cb').ztcolorbox();
    $('.view_friends').ztcolorbox( view_friends_callback.init );

    })(jQuery)





