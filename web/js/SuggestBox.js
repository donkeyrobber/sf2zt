(function($){

    var goals = [];
            
    $.each(suggestions,function(i,g){
            
        goals.push( new Goal(g) );    
            
    });

    if( typeof(goals !== 'undefined') && goals.length>0){
     
         var box = $("<div class='goal_suggest'></div>");
         var header = $("<h3>Other goals you may be interested in</h3>");
         
         var sug = "";
         
  
        $.each(goals, function(i,goal){
         
         sug += "<div class='row'>";
             sug += "<div class='four columns'>";
                 sug += "<img src='"+ goal.image +"' />";  
             sug += "</div>";
             sug += "<div class='eight columns'>";
                 sug += "<h3><a href='"+Routing.generate('goal_view', {'goal_id': goal.id})+"'>"+goal.title+"</a></h3>";
             sug += "</div>";
         sug += "</div>";   
         sug += "<div class='row'>"; 
             sug += "<div class='six columns'>";
                 sug += "<span><a href='"+Routing.generate('profile', {'id': goal.user.profile_identifier})+"'>"+goal.user.identifier+"</a></span>";
             sug += "</div>"; 
             sug += "<div class='six columns'>";
                 sug += "<span>";
                 
                 $.each(NG.getImage(goal.getDaysRemaining()), function(i,image){
                 
                     sug += "<img src='"+image+"' alt='' title='Days Remaining' />";
                 
                 })  
 
                 sug += "</span>";            
              sug += "</div>"; 
         sug += "</div>";                                 
        });
        
        sug = $(sug);
        
        box.append(header).append( sug );
        
        $.colorbox({'html':box});     
     }





})(jQuery)