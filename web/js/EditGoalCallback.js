var edit_goal_callback = {

    'init' : function(){

        $('#edit_goal_iframe').on('load',edit_goal_callback.load);

    },
    'load' : function(){

        var access_level = ( $('#goal_access_level_3').is(':checked') ) ? 'public' : 'private';

        var selector = '#'+ access_level + '_goals';

        var html  = $('#edit_goal_iframe').contents().find(selector).html();

        $.colorbox.close();

        $(selector).html(html);

        $(selector+' .edit_goal').ztcolorbox(edit_goal_callback.init );

        $(selector+' .cb').ztcolorbox();

        return false;

    }



}
