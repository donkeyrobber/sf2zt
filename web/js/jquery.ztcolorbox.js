$.fn.ztcolorbox  = $.ztcolorbox  = function( options ){

    var $options={'transition': 'fade',
                'opacity': 0.4,
                'overlayClose':false,
                'width':'60%' ,
                'fastIframe':false};
                
    var $this = this;  
      
    switch( typeof( options ) ){
    
        case "object":
       
        for( i in options){

            $options[i]=options[i];
        
        }      

        break;
        case "function":
        
            $options.onComplete = options;
            
        break;

         
    
    }


    if( ! $this[0]){

        if(! $options.href){

            return $this;
        
        }
    
        $options.open = true ;    

        $.colorbox($options);
    }
    
    if( ! typeof( $this[0] ) === 'array' ){
 
        $this[0] = [$this[0]];
 
    };
 
    $.each($this,function(i,e){
       
       if( typeof(e) === "object" ) {
       
      //  $(this).off('click.ztcolorbox');
         
         $(this).on('click.ztcolorbox', function( ){  
               
             if( typeof($(this).attr('href')) !== "undefined" ){

                 $options.href = $(this).attr('href');
             }
            $.colorbox($options);
                
            return false;
        
        });   
       }
    });
// 
//      
//     
    return $this;


}


