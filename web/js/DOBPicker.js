(function($){
    
    $('#registration_user_date_of_birth').datepicker({changeMonth: true,
                                                            changeYear: true,
                                                            yearRange: '1910:2000',
                                                            dateFormat: 'dd-mm-yy'});
    
})(jQuery)