var view_goal_callback = {
        
        goal_id : null,
        
        view_goal_container : null,
        
        'setHandlers' : function(){
        
            $('#form_post').on('submit',view_goal_callback.put);    
            $('.support_goal').on('click',view_goal_callback.support);
            $('form.delete_post').live('submit',view_goal_callback.delete_post );        
            $('.view_goal').on('click',view_goal_callback.init);
            $('.edit_goal').ztcolorbox(edit_goal_callback.init );

        },
         
        'init' : function(){

            view_goal_callback.goal_id = $('#Post_goal').val();
            
            view_goal_callback.view_goal_container = $('#cb_wrapper');  
            
            view_goal_callback.setHandlers();
        },
        'view' : function(html){
           
          $('#cb_wrapper').html(html);
             
          $(this).colorbox.resize();

        },
        'put' : function(){
        
            var data = { 'url' : $(this).attr('action'),
                       'type' : 'put', 
                       'data' : {
                                    'goal_id' : $('#Post_goal').val(),
                                    'comment' : $('#Post_comment').val(),
                                    '_method' : 'put'
                                },
                       'success' : view_goal_callback.reload
                     };
          
            $.ajax(data); 
                    
            return false;        
        
        },
        
        'delete_post' : function(){
        
            var post_id = $(this).find("input[name='postControl[id]']").val();
            
            var data = { 'url' : $(this).attr('action'),
                       'type' : 'delete', 
                       'data' : {
                                    'post_id' : post_id,
                                },
                       'success' : view_goal_callback.reload
                     };
          
            $.ajax(data);  
            
            return false;
        
        },
        'support' : function(){
        
            var data = {
            
                'url' : $(this).attr('href'),
                
                'type' : 'get' ,
                
                'success' : view_goal_callback.reload
            }
        
            $.ajax(data);
            
            return false;
        },
        'reload' : function(){
        
            var url = Routing.generate('goal_view',{'goal_id' : view_goal_callback.goal_id});
            
            var data = {
            
                'url' : url,
                'type' : 'get',
                'success' : function(html){
                
                    view_goal_callback.view_goal_container.html(html);
                    
                    view_goal_callback.setHandlers();                   
        
                }
                              
            
            };
            
            $.ajax(data);
            
            return false;
        
        }
        
    
};