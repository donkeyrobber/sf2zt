         var NG = {
           
               'image_size' : 'small',
               
               'image_path' : '/img/numbers',
               
               'image_type' : 'png',
           
               'getImage' : function(number){
                             
                   if( typeof(number) !== 'number'){
                   
                       return number;
                   
                   }
                   
                   var image_path = this.image_path +'/'+ this.image_size;
                
                   var number_str = String(number);
                
                   var number_arr = number_str.split('');
                
                   var image_array = [];
                
                   var img;
                
                   for(var i=0;i<number_arr.length;i++){
                
                       img = image_path + "/" + number_arr[i] + "."+this.image_type;    
                
                       image_array.push(img);
                
                   } 
                
                   return image_array;  
               
               }
           
           }