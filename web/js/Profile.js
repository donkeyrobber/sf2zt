 (function($){

//    if( typeof(show_suggest) !== 'undefined' ){
//
//        $.ztcolorbox({ 'href' : Routing.generate( 'goal_suggest') });
//
//    }
//
  
    
    var add_goal_callback = {
    
        init : function(){
        
            $('#add_goal_iframe').on('load',add_goal_callback.load);
        
        },
        load : function(){
        
            var html  = $('#add_goal_iframe').contents().find('#cb_wrapper').html();
            
        //    $('#cb_wrapper').html(html);
            
        //    $('#add_goal_container .cb').ztcolorbox();
        
        }
    
    }
    
    var edit_profile_callback = {
    
        'init' : function(){
            
            $('#edit_profile_iframe').on('load',edit_profile_callback.close);                
        
        },
        'close': function(){

            var html  = $('#edit_profile_iframe').contents().find('#profile').html();

            $('#profile_container').empty();
            
            $.colorbox.close();
            
            $('#profile_container').html(html);
            
            $('.edit_profile').ztcolorbox(edit_profile_callback.init );
            
                       
            
            return false;
        }
         
        
    
    }

    var create_message_callback = {

        'init' : function(){

            $('form#create_message').on('submit',create_message_callback.send)

        },
        'send' : function(){

            var data = { 'url' : $(this).attr('action'),
                'type' : 'put',
                'data' : {
                    'to' : $('#message_to').val(),
                    'subject' : $('#message_subject').val(),
                    'body' : $('#message_body').val(),
                    '_method' : 'put'
                },
                'success' : create_message_callback.close
            };

            $.ajax(data);

            return false;
        },
        'close': function(){

            $.colorbox.close();

            return true;

        }

    }

    var view_message_callback = {

        'init' : function(){

            $('.create_message').ztcolorbox(create_message_callback.init);
            $('.delete_message').ztcolorbox();
        }

    }

    $('.cb').ztcolorbox();

     $('div.view_goal').each(function(){
         $(this).ztcolorbox(
             {'href': Routing.generate( 'goal_view',{'goal_id': $(this).find('input#goal_id').val() }),
                 'onComplete':view_goal_callback.init}); });
    
    $('.add_goal').ztcolorbox(add_goal_callback.init );

  //  $('.edit_goal').ztcolorbox(edit_goal_callback.init );
    
    $('.edit_profile').ztcolorbox(edit_profile_callback.init );

    $('.create_message').ztcolorbox(create_message_callback.init);

    $('.view_message').each(function(){ $(this).ztcolorbox({'href': Routing.generate( 'message_view',{'id': $(this).find('input.message_id').val() }),
    'onComplete': view_message_callback.init}) });




})(jQuery)